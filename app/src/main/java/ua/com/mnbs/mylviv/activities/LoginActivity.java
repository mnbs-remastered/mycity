package ua.com.mnbs.mylviv.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.com.mnbs.mylviv.R;
import ua.com.mnbs.mylviv.models.User;

public class LoginActivity extends AppCompatActivity {

    private GoogleApiClient mGoogleApiClient;
    private FirebaseAuth mAuth;
    private static final int RC_SIGN_IN = 91;
    private CallbackManager mCallbackManager;
    private String facebookUserId = "";

    @BindView(R.id.login_email)
    EditText loginEmail;
    @BindView(R.id.login_password)
    EditText loginPassword;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.activity_authentication)
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder((GoogleSignInOptions.DEFAULT_SIGN_IN))
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .requestProfile()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this).
                enableAutoManage(this, connectionResult ->
                        Toast.makeText(LoginActivity.this, "Немає з'єднання з інтернетом", Toast.LENGTH_LONG)
                                .show()).
                addApi(Auth.GOOGLE_SIGN_IN_API, gso).
                build();
        ButterKnife.bind(this);

        mCallbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = findViewById(R.id.btn_facebook);
        loginButton.setReadPermissions("email", "public_profile");

        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("TAG", "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
                progressBar.setVisibility(ProgressBar.VISIBLE);
                linearLayout.setVisibility(RelativeLayout.INVISIBLE);
            }

            @Override
            public void onCancel() {
                Log.d("TAG", "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("TAG", "facebook:onError", error);
                Toast.makeText(LoginActivity.this, R.string.login_failed + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @OnClick(R.id.btn_sign_in)
    public void signInApp() {
        String email = loginEmail.getText().toString();
        String password = loginPassword.getText().toString();
        if (!email.equals("") && !password.equals("")) {
            progressBar.setVisibility(ProgressBar.VISIBLE);
            linearLayout.setVisibility(RelativeLayout.INVISIBLE);
            signIn(email, password);
        } else if (email.equals("") && !password.equals("")) {
            Toast.makeText(LoginActivity.this, R.string.email_field_required, Toast.LENGTH_LONG).show();
            progressBar.setVisibility(ProgressBar.INVISIBLE);
            linearLayout.setVisibility(RelativeLayout.VISIBLE);
        } else if (!email.equals("")) {
            Toast.makeText(LoginActivity.this, R.string.password_field_required, Toast.LENGTH_LONG).show();
            progressBar.setVisibility(ProgressBar.INVISIBLE);
            linearLayout.setVisibility(RelativeLayout.VISIBLE);
        } else {
            Toast.makeText(LoginActivity.this, "Ну и как же, вашу мать, надо ето понимать", Toast.LENGTH_LONG).show();
            progressBar.setVisibility(ProgressBar.INVISIBLE);
            linearLayout.setVisibility(RelativeLayout.VISIBLE);
        }
    }

    @OnClick(R.id.btn_google_group)
    public void registerViaGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
        progressBar.setVisibility(ProgressBar.VISIBLE);
        linearLayout.setVisibility(RelativeLayout.INVISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                GoogleSignInAccount account = result.getSignInAccount();
                authWithGoogle(account);
                progressBar.setVisibility(ProgressBar.VISIBLE);
                linearLayout.setVisibility(RelativeLayout.INVISIBLE);
            } else {
                Toast.makeText(LoginActivity.this, "Помилка входу:" + Auth.GoogleSignInApi.getSignInResultFromIntent(data)
                        .getStatus().toString(), Toast.LENGTH_LONG).show();
                progressBar.setVisibility(ProgressBar.INVISIBLE);
                linearLayout.setVisibility(RelativeLayout.VISIBLE);
            }
        } else {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void authWithGoogle(GoogleSignInAccount account) {
        loginWithCredential(GoogleAuthProvider.getCredential(account.getIdToken(), null));
    }

    private void handleFacebookAccessToken(AccessToken token) {
        loginWithCredential(FacebookAuthProvider.getCredential(token.getToken()));
    }

    private void loginWithCredential(AuthCredential credential) {
        mAuth.signInWithCredential(credential).addOnCompleteListener(this, task -> {
            if (task.isSuccessful()) {
                    handleDocumentCreation();
            } else {
                Log.w("TAG", "signInWithCredential:failure", task.getException());
                Toast.makeText(LoginActivity.this, "Authentication failed." + task.getException().getMessage(),
                        Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(ProgressBar.INVISIBLE);
                linearLayout.setVisibility(RelativeLayout.VISIBLE);
            }
        });
    }

    private void handleDocumentCreation() {
        FirebaseFirestore.getInstance().collection("UsersList").document(mAuth.getCurrentUser().getUid())
                .get().addOnSuccessListener( documentSnapshot -> {
                User user = documentSnapshot.toObject(User.class);
                if (user == null) {
                    FirebaseUser currentUser = mAuth.getCurrentUser();
                    User newUser = new User();
                    newUser.setEmail(currentUser.getEmail());
                    newUser.setImageUrl(currentUser.getPhotoUrl().toString());
                    for (UserInfo profile : currentUser.getProviderData()) {
                        if (FacebookAuthProvider.PROVIDER_ID.equals(profile.getProviderId())) {
                            facebookUserId = profile.getUid();
                            mAuth.getCurrentUser().updateProfile(new UserProfileChangeRequest.Builder()
                                    .setPhotoUri(Uri.parse("https://graph.facebook.com/" + facebookUserId + "/picture?height=500"))
                                    .build()).addOnSuccessListener(aVoid -> newUser.setImageUrl(currentUser.getPhotoUrl().toString())
                            ).addOnFailureListener(e -> Log.e("Error", e.getMessage(), e));
                            break;
                        }
                    }
                    newUser.setName(currentUser.getDisplayName());
                    newUser.setCity("Львів");
                    newUser.setId(currentUser.getUid());
                    HashMap<String, Boolean> settings = new HashMap<>();
                    settings.put(getString(R.string.water_cut_off),true);
                    settings.put(getString(R.string.gas_cut_off),true);
                    settings.put(getString(R.string.electricity_cut_off),true);
                    settings.put(getString(R.string.road_problems),true);
                    settings.put(getString(R.string.transport_problems),true);
                    settings.put(getString(R.string.important_events),true);
                    newUser.setSettings(settings);
                    FirebaseFirestore.getInstance().collection("UsersList").document(currentUser.getUid())
                            .set(newUser).addOnSuccessListener( aVoid -> {
                        Intent intent = new Intent(LoginActivity.this, AdditionalRegistrationActivity.class);
                        startActivity(intent);
                        progressBar.setVisibility(ProgressBar.VISIBLE);
                        linearLayout.setVisibility(RelativeLayout.INVISIBLE);
                    }).addOnFailureListener( e -> {
                        Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();
                        mAuth.signOut();
                    });
                } else {
                    Intent intent = new Intent(LoginActivity.this, BaseActivity.class);
                    startActivity(intent);
                    progressBar.setVisibility(ProgressBar.VISIBLE);
                    linearLayout.setVisibility(RelativeLayout.INVISIBLE);
                }
        });
    }

    @OnClick(R.id.btn_registration)
    public void registrate() {
        Intent goToRegistration = new Intent(LoginActivity.this, RegistrationActivity.class);
        startActivity(goToRegistration);
    }

    @OnClick(R.id.password_forgotten)
    public void resetPassword() {
        Intent resetPassword = new Intent(LoginActivity.this, ResetPasswordActivity.class);
        startActivity(resetPassword);
    }

    private void signIn(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, task -> {
            if (task.isSuccessful()) {
                Intent intent = new Intent(LoginActivity.this, BaseActivity.class);
                startActivity(intent);
                progressBar.setVisibility(ProgressBar.VISIBLE);
                linearLayout.setVisibility(RelativeLayout.INVISIBLE);
            } else {
                Toast.makeText(LoginActivity.this, getLoginErrorMessage(task), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(ProgressBar.INVISIBLE);
                linearLayout.setVisibility(RelativeLayout.VISIBLE);
            }
        });
    }

    private String getLoginErrorMessage(Task task) {
        switch (task.getException().getMessage()) {
            case ("The password is invalid or the user does not have a password."):
                return "Неправильний пароль";
            case ("There is no user record corresponding to this identifier. The user may have been deleted."):
                return "Неправильний email. Введіть коректний email або зареєструйтеся";
        }
        return "Помилка з'єднання із сервером";
    }

    @Override
    public void finish() {

    }
}