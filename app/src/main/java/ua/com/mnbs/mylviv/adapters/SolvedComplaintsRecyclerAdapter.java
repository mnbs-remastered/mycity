package ua.com.mnbs.mylviv.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.com.mnbs.mylviv.R;
import ua.com.mnbs.mylviv.activities.BaseActivity;
import ua.com.mnbs.mylviv.fragments.ComplaintStatusFragment;
import ua.com.mnbs.mylviv.managers.FragmentManager;
import ua.com.mnbs.mylviv.managers.ImageSetter;
import ua.com.mnbs.mylviv.models.Complaint;

public class SolvedComplaintsRecyclerAdapter extends RecyclerView.Adapter<SolvedComplaintsRecyclerAdapter.ComplaintViewHolder> {

    public static final int MIN_SIZE_OF_DESCR_TO_BE_SHORTENED = 15;
    private Context mContext;
    private List<Complaint> mComplaints;
    private BaseActivity mActivity;


    public SolvedComplaintsRecyclerAdapter(Context context, List<Complaint> complaints) {
        mContext = context;
        mActivity = (BaseActivity) context;
        mComplaints = complaints;
    }


    @Override
    public ComplaintViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.solved_complaints_list_item, parent, false);
        return new ComplaintViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ComplaintViewHolder holder, int position) {
        Complaint currentComplaint = mComplaints.get(position);
        holder.type.setText(currentComplaint.getTitle());

        String description = currentComplaint.getDescription();
        if (description.length() < MIN_SIZE_OF_DESCR_TO_BE_SHORTENED) {
            holder.description.setText(description);
        } else {
            description = description.substring(0, MIN_SIZE_OF_DESCR_TO_BE_SHORTENED) + "<i>... більше</i>";
            holder.description.setText(Html.fromHtml(description));
            holder.description.setOnClickListener(v -> {
                ((TextView) v).setText(currentComplaint.getDescription());
                v.setOnClickListener(v1 -> holder.viewComplaint());
            });
        }

        Log.i("image:", currentComplaint.getImageUrl());
        ImageSetter.forComplaints().setImage(currentComplaint.getImageUrl(),
                holder.image, holder.progressBar);
    }

    private void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }

    @Override
    public long getItemId(int position) {
        Complaint complaint = mComplaints.get(position);
        return complaint.hashCode();
    }

    @Override
    public int getItemCount() {
        return mComplaints.size();
    }

    public class ComplaintViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.solved_complaints_title) TextView type;
        @BindView(R.id.solved_complaints_description) TextView description;
        @BindView(R.id.solved_complaints_image) ImageView image;
        @BindView(R.id.progressBar) ProgressBar progressBar;

        public ComplaintViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.solved_complaints_list_item)
        public void viewComplaint() {
            Bundle bundle = new Bundle();
            bundle.putString("solved", "From SolvedComplaints");
            ComplaintStatusFragment complaintStatusFragment = new ComplaintStatusFragment();
            complaintStatusFragment.setArguments(bundle);
            Complaint chosenComplaint = mComplaints.get(getPosition());
            Fragment fragment = new ComplaintStatusFragment();
            bundle.putSerializable("complaint", chosenComplaint);
            fragment.setArguments(bundle);

            FragmentManager.open(fragment, mActivity);
        }
    }
}
