package ua.com.mnbs.mylviv.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.LocationRestriction;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.Unbinder;
import ua.com.mnbs.mylviv.R;
import ua.com.mnbs.mylviv.activities.AdditionalRegistrationActivity;
import ua.com.mnbs.mylviv.activities.BaseActivity;
import ua.com.mnbs.mylviv.adapters.PlaceAdapter;
import ua.com.mnbs.mylviv.adapters.SettingsAdapter;
import ua.com.mnbs.mylviv.models.City;
import ua.com.mnbs.mylviv.models.Settings;
import ua.com.mnbs.mylviv.models.User;

public class SettingsFragment extends Fragment {
    private Unbinder unbinder;
    private ArrayList<Settings> settingsList = new ArrayList<>();
    private ArrayList<String> placesList = new ArrayList<>();
    private HashMap<String, Boolean> updatedSettings = new HashMap<>();
    private SettingsAdapter adapter;
    private int height;
    private CollectionReference reference;
    private String uid;

    @BindView(R.id.settings_add_location)
    LinearLayout addLocationButton;
    @BindView(R.id.settings_list)
    ListView listView;
    @BindView(R.id.list_of_chosen_places)
    ListView placesListView;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;
    @BindView(R.id.typeOfNotifications)
    LinearLayout typeOfNotificationsLayout;
    @BindView(R.id.listOfPlacesLayout)
    LinearLayout listOfPlacesLayout;
    @BindView(R.id.locationLayout)
    FrameLayout locationLayout;
    @BindView(R.id.idCardView)
    CardView addPlaceCardView;
    @BindView(R.id.settings_spinner_city)
    Spinner citySpinner;
    @BindView(R.id.street_text_view)
    TextView streetTextView;
    @BindView(R.id.txtView)
    TextView txtView;
    @BindView(R.id.save_changes)
    Button saveChangesButton;
    @BindView(R.id.submit_info)
    Button submitAdditionalInfo;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        settingsList.clear();
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity())
                    .setActionBarTitle(R.string.settings_title);
        }
        return inflater.inflate(R.layout.fragment_settings, null);
    }

    String TAG = "placeautocomplete";

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        unbinder = ButterKnife.bind(this, view);

        reference = FirebaseFirestore.getInstance().collection("UsersList");
        uid = FirebaseAuth.getInstance().getCurrentUser().getUid();

        if (getActivity() instanceof AdditionalRegistrationActivity) {
            submitAdditionalInfo.setVisibility(View.VISIBLE);
        }

        final ArrayAdapter<City> citiesSpinnerArrayAdapter = new ArrayAdapter<City>(
                getActivity().getApplicationContext(), R.layout.spinner_item, City.values()) {
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position % 2 == 1) {
                    tv.setBackgroundColor(Color.parseColor("#FFFFFF"));
                } else {
                    tv.setBackgroundColor(Color.parseColor("#FFFFFF"));
                }
                if (position == 0) {
                    tv.setBackgroundColor(Color.parseColor("#E1E1E1"));
                }
                return view;
            }
        };

        citySpinner.setPrompt("Оберіть місто");

        citiesSpinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        citySpinner.setAdapter(citiesSpinnerArrayAdapter);

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String cityTextView = citySpinner.getSelectedItem().toString().trim();
                if (cityTextView.equalsIgnoreCase("Львів")) {
                    setPlaceForLviv();
                } else if (cityTextView.equalsIgnoreCase("Київ")) {
                    setPlaceForKyiv();
                } else if (cityTextView.equalsIgnoreCase("Дніпро")) {
                    setPlaceForDnipro();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) { }
        });

        settingsList.add(new Settings(R.string.water_cut_off, R.drawable.water));
        settingsList.add(new Settings(R.string.gas_cut_off, R.drawable.gas));
        settingsList.add(new Settings(R.string.electricity_cut_off, R.drawable.electricity));
        settingsList.add(new Settings(R.string.road_problems, R.drawable.road));
        settingsList.add(new Settings(R.string.transport_problems, R.drawable.transport));
        settingsList.add(new Settings(R.string.important_events, R.drawable.importantevents));
        adapter = new SettingsAdapter(getActivity(), settingsList);

        FirebaseFirestore.getInstance()
                .collection("UsersList")
                .document(FirebaseAuth.getInstance()
                        .getCurrentUser().getUid())
                .addSnapshotListener((documentSnapshot, e) -> {
                    User user = documentSnapshot.toObject(User.class);
                    placesList = user.getAddresses();
                    HashMap<String, Boolean> settingsHashMap = user.getSettings();
                    adapter.setSettingsHashMap(settingsHashMap);

                    if (getContext() != null) {
                        if (placesList != null) {
                            PlaceAdapter placeAdapter = new PlaceAdapter(getActivity(), placesList);
                            placesListView.setAdapter(placeAdapter);
                            PlaceAdapter.setPlaces(placesList);
                            calculateHeight(placesListView);
                        }
                    }
                    listView.setAdapter(adapter);
                });

        swipeContainer.setOnRefreshListener(() -> {
            FragmentManager fm = getActivity().getSupportFragmentManager();
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            SettingsFragment newFragment = new SettingsFragment();
            ft.replace(R.id.container, newFragment, "SettingsFragment");
            ft.addToBackStack(null);
            ft.commit();
            fm.popBackStack();
            swipeContainer.setRefreshing(false);
        });

        swipeContainer.setColorSchemeResources(R.color.colorPrimary);
        setFadeAnimation(typeOfNotificationsLayout);
        setFadeAnimation(listOfPlacesLayout);

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition = (listView == null || listView.getChildCount() == 0) ? 0 : listView.getChildAt(0).getTop();
                swipeContainer.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }
        });
    }

    private static void calculateHeight(ListView listView) {

        ListAdapter mAdapter = listView.getAdapter();

        int totalHeight = 0;

        for (int i = 0; i < mAdapter.getCount(); i++) {
            View mView = mAdapter.getView(i, null, listView);

            mView.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),

                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

            totalHeight += mView.getMeasuredHeight();
            Log.w("HEIGHT" + i, String.valueOf(totalHeight));

        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (mAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnItemClick(R.id.settings_list)
    public void setCheckBoxChecked(View v) {
        CheckBox checkBox = v.findViewById(R.id.settings_checkbox_problem);
        checkBox.setChecked(!checkBox.isChecked());
    }

    @OnClick(R.id.settings_add_location)
    public void onAddLocationButtonClick() {
        citySpinner.setSelection(0);

        addPlaceCardView.setVisibility(View.GONE);
        streetTextView.setVisibility(View.GONE);
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getChildFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        autocompleteFragment.a.setText("");

        locationLayout.setVisibility(View.VISIBLE);
        setFadeAnimationForPlace(locationLayout);
    }

    private void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(1500);
        view.startAnimation(anim);
    }

    private void setFadeAnimationForPlace(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(800);
        view.startAnimation(anim);
    }

    public ListView getListView() {
        return listView;
    }

    public void setPlaceForLviv() {
        setPlace(RectangularBounds.newInstance(
                new LatLng(49.768950, 23.929387),
                new LatLng(49.889275, 24.151218)));

    }

    public void setPlaceForKyiv() {
        setPlace(RectangularBounds.newInstance(
                new LatLng(50.193968, 30.233437),
                new LatLng(50.592412, 30.831734)));
    }

    public void setPlaceForDnipro() {
        setPlace(RectangularBounds.newInstance(
                new LatLng(48.367987, 34.882024),
                new LatLng(48.512268, 35.155841)));
    }

    public void setPlace(LocationRestriction restriction) {
        streetTextView.setVisibility(View.VISIBLE);
        addPlaceCardView.setVisibility(View.VISIBLE);
        String city = citySpinner.getSelectedItem().toString();

        Places.initialize(getActivity().getApplicationContext(), "AIzaSyBnL3WSWzhh-PgdIg9wpWRk-HZTz6GUjU0");
        PlacesClient placesClient = Places.createClient(getContext());

        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getChildFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        autocompleteFragment.setLocationRestriction(restriction);
        autocompleteFragment.setTypeFilter(TypeFilter.ADDRESS);
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME));

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onPlaceSelected(Place place) {
                FirebaseFirestore.getInstance()
                        .collection("UsersList")
                        .document(FirebaseAuth.getInstance()
                                .getCurrentUser().getUid())
                        .get().addOnSuccessListener(documentSnapshot -> {
                    User user = documentSnapshot.toObject(User.class);
                    ArrayList<String> addresses = user.getAddresses();
                    addresses.add(txtView.getText().toString());

                    FirebaseFirestore.getInstance()
                            .collection("UsersList")
                            .document(user.getId())
                            .update("addresses", addresses);
                });
                txtView.setText(place.getName() + ", " + city);
                locationLayout.setVisibility(View.GONE);
            }

            @Override
            public void onError(Status status) {
                Log.i(TAG, "An error occurred: " + status);
            }
        });
    }

    @SuppressLint("ResourceAsColor")
    @OnClick(R.id.save_changes)
    public void saveNotification() {
        saveChangesButton.setTextColor(R.color.colorPrimary);
        HashMap<String, Object> update = new HashMap<>();
        for (int i = 0; i < listView.getChildCount(); i++) {
            CheckBox checkBox = listView.getChildAt(i).findViewById(R.id.settings_checkbox_problem);
            TextView textView = listView.getChildAt(i).findViewById(R.id.settings_list_item_problem);
            updatedSettings.put(textView.getText().toString(), checkBox.isChecked());
        }
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        SettingsFragment newFragment = new SettingsFragment();
        ft.replace(R.id.container, newFragment, "SettingsFragment");
        ft.addToBackStack(null);
        ft.commit();
        fm.popBackStack();

        update.put("settings", updatedSettings);

        FirebaseFirestore.getInstance()
                .collection("UsersList")
                .document(FirebaseAuth.getInstance()
                        .getCurrentUser().getUid())
                .update(update);
    }

    public void setVisibilityOfButtonChanges() {
        saveChangesButton.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.submit_info)
    public void submitAdditionalInfo() {
        HashMap<String, Object> update = new HashMap<>();
        for (int i = 0; i < listView.getChildCount(); i++) {
            CheckBox checkBox = listView.getChildAt(i).findViewById(R.id.settings_checkbox_problem);
            TextView textView = listView.getChildAt(i).findViewById(R.id.settings_list_item_problem);
            updatedSettings.put(textView.getText().toString(), checkBox.isChecked());
        }

        update.put("settings", updatedSettings);

        FirebaseFirestore.getInstance()
                .collection("UsersList")
                .document(FirebaseAuth.getInstance()
                        .getCurrentUser().getUid())
                .update(update);

        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
                        R.anim.slide_in_left, R.anim.slide_out_right)
                .replace(R.id.container, new AccountSettingsFragment())
                .commit();
    }
}
