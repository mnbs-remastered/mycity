package ua.com.mnbs.mylviv.managers;

import android.support.v4.app.Fragment;

import ua.com.mnbs.mylviv.R;
import ua.com.mnbs.mylviv.activities.BaseActivity;

public class FragmentManager {

    public static void open(Fragment fragment, BaseActivity activity) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
                        R.anim.slide_in_left, R.anim.slide_out_right)
                .replace(R.id.container, fragment)
                .commit();
    }

    public static Fragment getCurrentFragment(BaseActivity activity) {
        return activity.getSupportFragmentManager().findFragmentById(R.id.container);
    }
}
