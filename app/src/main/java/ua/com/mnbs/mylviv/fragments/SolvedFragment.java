package ua.com.mnbs.mylviv.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ProgressBar;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ua.com.mnbs.mylviv.R;
import ua.com.mnbs.mylviv.activities.BaseActivity;
import ua.com.mnbs.mylviv.adapters.SolvedComplaintsRecyclerAdapter;
import ua.com.mnbs.mylviv.managers.Transliterator;
import ua.com.mnbs.mylviv.models.Complaint;
import ua.com.mnbs.mylviv.models.Notification;


public class SolvedFragment extends Fragment {

    private Unbinder unbinder;

    FirebaseFirestore firestore;

    @BindView(R.id.solved_complaints_list)
    RecyclerView solvedRecyclerView;
    @BindView(R.id.solved_frag_progressBar)
    ProgressBar progressBar;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;
    @BindView(R.id.empty_view)
    View emptyView;

    private LayoutInflater inflater;
    @Nullable
    private ViewGroup container;
    @Nullable
    private Bundle savedInstanceState;

    DocumentSnapshot lastVisible;
    boolean isScrolling;
    boolean isLastItemReached = false;
    public static final int QUERY_PAGE_LENGHT = 10;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.inflater = inflater;
        this.container = container;
        this.savedInstanceState = savedInstanceState;
        return inflater.inflate(R.layout.fragment_solved, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        FirebaseApp.initializeApp(getActivity());

        unbinder = ButterKnife.bind(this, view);

        firestore = FirebaseFirestore.getInstance();
        firestore.collection("UsersList").document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .get().addOnSuccessListener(documentSnapshot -> setAdapter((String) documentSnapshot.get("city")));

        progressBar.setVisibility(ProgressBar.INVISIBLE);
        swipeContainer.setOnRefreshListener(() -> {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            FragmentManager fm = getActivity().getSupportFragmentManager();
            SolvedFragment newFragment = new SolvedFragment();
            ft.addToBackStack(null);
            ft.replace(R.id.container, newFragment, "NotificationFragment");
            ft.commit();
            fm.popBackStack();
            ((BaseActivity) getActivity())
                    .setActionBarTitle(R.string.solved_complaints_title);
            swipeContainer.setRefreshing(false);
        });

        swipeContainer.setColorSchemeResources(R.color.colorPrimary);

    }

    private void setAdapter(String city) {
        firestore.collection("Cities").document(Transliterator.transiterate(city))
                .collection("Complaints").whereEqualTo("status", "Вирішено").orderBy("date").limit(QUERY_PAGE_LENGHT)
                .get().addOnSuccessListener(queryDocumentSnapshots -> {
                    ArrayList<Complaint> complaints = new ArrayList<>();
                    for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Complaint complaint = documentSnapshot.toObject(Complaint.class);
                        complaints.add(complaint);
                    }
                    if (complaints.size() == 0) {
                        emptyView.setVisibility(View.VISIBLE);
                        return;
                    }

                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                    solvedRecyclerView.setLayoutManager(linearLayoutManager);
                    SolvedComplaintsRecyclerAdapter adapter = new SolvedComplaintsRecyclerAdapter(getActivity(), complaints);
                    ((DefaultItemAnimator) solvedRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
                    solvedRecyclerView.setAdapter(adapter);

                    lastVisible = queryDocumentSnapshots.getDocuments().get(complaints.size() - 1);

                    solvedRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);

                            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                                isScrolling = true;
                            }
                        }

                        @Override
                        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                            int firstItem = linearLayoutManager.findFirstVisibleItemPosition();
                            int visibleItemCount = linearLayoutManager.getChildCount();
                            int totalItemCount = linearLayoutManager.getItemCount();
                            RecyclerView.ItemAnimator animator = solvedRecyclerView.getItemAnimator();
                            if (animator instanceof SimpleItemAnimator) {
                                ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
                            }

                            if (isScrolling & (firstItem + visibleItemCount == totalItemCount) & !isLastItemReached) {
                                isScrolling = false;
                                firestore.collection("PendingComplaintsList").whereEqualTo("status", "Вирішено").orderBy("date")
                                        .startAfter(lastVisible).limit(QUERY_PAGE_LENGHT).get().addOnSuccessListener(newDocumentSnaphots -> {

                                    for (DocumentSnapshot documentSnapshot : newDocumentSnaphots) {
                                        Complaint complaint = documentSnapshot.toObject(Complaint.class);
                                        complaints.add(complaint);
                                    }

                                    adapter.notifyDataSetChanged();

                                    if (newDocumentSnaphots.size() > 0) {
                                        lastVisible = newDocumentSnaphots.getDocuments().get(newDocumentSnaphots.size() - 1);
                                    } else {
                                        recyclerView.removeOnScrollListener(this);
                                    }
                                    if (newDocumentSnaphots.getDocumentChanges().size() < QUERY_PAGE_LENGHT) {
                                        recyclerView.removeOnScrollListener(this);
                                    }
                                });
                            }
                        }
                    });
                }).addOnFailureListener( e -> Log.e("error",e.getLocalizedMessage()));
    }
}
