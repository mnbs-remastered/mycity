package ua.com.mnbs.mylviv.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ua.com.mnbs.mylviv.R;
import ua.com.mnbs.mylviv.activities.BaseActivity;
import ua.com.mnbs.mylviv.adapters.MyComplaintsRecyclerAdapter;
import ua.com.mnbs.mylviv.managers.Transliterator;
import ua.com.mnbs.mylviv.models.Complaint;

public class MyComplaintsFragment extends Fragment {

    private FirebaseAuth mAuth;

    private Unbinder unbinder;

    @BindView(R.id.my_complaints_list)
    RecyclerView recyclerView;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;
    @BindView(R.id.empty_view)
    View emptyView;

    private LayoutInflater inflater;
    @Nullable
    private ViewGroup container;
    @Nullable
    private Bundle savedInstanceState;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.inflater = inflater;
        this.container = container;
        this.savedInstanceState = savedInstanceState;
        return inflater.inflate(R.layout.fragment_my_complaints, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        FirebaseApp.initializeApp(getActivity());

        unbinder = ButterKnife.bind(this, view);

        mAuth = FirebaseAuth.getInstance();
        FirebaseFirestore.getInstance().collection("UsersList").document(mAuth.getCurrentUser().getUid())
                .get().addOnSuccessListener(documentSnapshot -> {
                    String city = (String) documentSnapshot.get("city");
            FirebaseFirestore.getInstance().collection("Cities").document(Transliterator.transiterate(city))
                    .collection("Complaints").whereEqualTo("user", mAuth.getCurrentUser().getUid())
                    .orderBy("date", Query.Direction.DESCENDING)
                    .addSnapshotListener((queryDocumentSnapshots, e) -> {
                        List<Complaint> myComplaints = queryDocumentSnapshots.toObjects(Complaint.class);
                        setAdapter(myComplaints);
                    });
        });

        swipeContainer.setOnRefreshListener(() -> {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            FragmentManager fm = getActivity().getSupportFragmentManager();
            MyComplaintsFragment newFragment = new MyComplaintsFragment();
            ft.addToBackStack(null);
            ft.replace(R.id.container, newFragment, "MyComplaintsFragment");
            ft.commit();
            fm.popBackStack();
            ((BaseActivity) getActivity())
                    .setActionBarTitle(R.string.my_complaints_title);
            swipeContainer.setRefreshing(false);
        });

        swipeContainer.setColorSchemeResources(R.color.colorPrimary);
    }

    private void setAdapter(List<Complaint> complaints) {
        if (getContext() != null) {
            if (complaints.isEmpty()) {
                emptyView.setVisibility(View.VISIBLE);
            } else {
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                recyclerView.setHasFixedSize(true);
                MyComplaintsRecyclerAdapter adapter = new MyComplaintsRecyclerAdapter(getActivity(), complaints);
                recyclerView.setAdapter(adapter);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
