package ua.com.mnbs.mylviv.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.com.mnbs.mylviv.R;
import ua.com.mnbs.mylviv.activities.BaseActivity;
import ua.com.mnbs.mylviv.fragments.ComplaintStatusFragment;
import ua.com.mnbs.mylviv.managers.FragmentManager;
import ua.com.mnbs.mylviv.managers.ImageSetter;
import ua.com.mnbs.mylviv.models.Complaint;

public class MyComplaintsRecyclerAdapter extends RecyclerView.Adapter<MyComplaintsRecyclerAdapter.ComplaintViewHolder> {

    public static final int MIN_SIZE_OF_DESCR_TO_BE_SHORTENED = 15;
    private int lastPosition = -1;
    private Context mContext;
    private List<Complaint> mComplaints;
    private BaseActivity mActivity;

    public MyComplaintsRecyclerAdapter(Context context, List<Complaint> complaints) {
        mContext = context;
        mActivity = (BaseActivity) context;
        mComplaints = sortUploadsByDate(complaints);
    }

    private List<Complaint> sortUploadsByDate(List<Complaint> complaints) {
        Collections.sort(complaints,this::compareDates);
        return complaints;
    }

    private int compareDates(Complaint p1, Complaint p2) {
        Date first = p1.getDate();
        Date second = p2.getDate();
        return second.compareTo(first);
    }

    @Override
    public ComplaintViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.my_complaints_list_item, parent, false);
        return new MyComplaintsRecyclerAdapter.ComplaintViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyComplaintsRecyclerAdapter.ComplaintViewHolder holder, int position) {
        Complaint currentComplaint = mComplaints.get(position);
        try {
            holder.type.setText(currentComplaint.getTitle());
            String description = currentComplaint.getDescription();
            if (description.length() < MIN_SIZE_OF_DESCR_TO_BE_SHORTENED) {
                holder.description.setText(description);
            } else {
                description = description.substring(0, MIN_SIZE_OF_DESCR_TO_BE_SHORTENED) + "<i>... більше</i>";
                holder.description.setText(Html.fromHtml(description));
                holder.description.setOnClickListener(v -> {
                    ((TextView) v).setText(currentComplaint.getDescription());
                    v.setOnClickListener(v1 -> holder.viewComplaint());
                });
            }
            holder.status.setText(currentComplaint.getStatus());

            Log.i("image:", currentComplaint.getImageUrl());
            ImageSetter.forComplaints().setImage(currentComplaint.getImageUrl(),
                    holder.image, holder.progressBar);
        } catch (NullPointerException e) {
            //TODO: to be deleted
        }
        //setAnimation(holder.itemView, position);
        setFadeAnimation(holder.itemView);
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.slide_in_right);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    private void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }

    @Override
    public int getItemCount() {
        return mComplaints.size();
    }

    public class ComplaintViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.my_complaint_title) TextView type;
        @BindView(R.id.my_complaint_description) TextView description;
        @BindView(R.id.my_complaint_image) ImageView image;
        @BindView(R.id.my_complaint_status) TextView status;
        @BindView(R.id.progressBar) ProgressBar progressBar;

        public ComplaintViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setLongClickable(true);
            itemView.setOnLongClickListener(v -> {
                Complaint complaint = mComplaints.get(getPosition());
                Log.e("TAG", "Activated");
                AtomicBoolean result = new AtomicBoolean(false);
                FirebaseFirestore.getInstance().collection("PendingComplaintsList").whereEqualTo("imageUrl", complaint.getImageUrl())
                        .get().addOnSuccessListener(queryDocumentSnapshots ->
                        queryDocumentSnapshots.getDocuments().get(0).getReference().delete().addOnSuccessListener(aVoid -> {
                            {
                                result.set(true);
                                Toast.makeText(mContext, "Скаргу Видалено", Toast.LENGTH_LONG).show();
                            }
                        })
                );
                return result.get();
            });
        }

        @OnClick(R.id.my_complaints_list_item)
        public void viewComplaint() {
            Bundle bundle = new Bundle();
            bundle.putString("My", "From MyComplaints");
            ComplaintStatusFragment complaintStatusFragment = new ComplaintStatusFragment();
            complaintStatusFragment.setArguments(bundle);
            Complaint chosenComplaint = mComplaints.get(getPosition());

            Fragment fragment = new ComplaintStatusFragment();
            bundle.putSerializable("complaint", chosenComplaint);
            fragment.setArguments(bundle);

            FragmentManager.open(fragment, mActivity);
        }
    }
}