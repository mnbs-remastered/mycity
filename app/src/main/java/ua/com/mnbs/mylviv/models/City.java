package ua.com.mnbs.mylviv.models;

public enum City {
    NOCITY("Обрати місто"),
    LVIV("Львів"),
    KYIV("Київ"),
    DNIPRO("Дніпро");

    private String text;

    City(String text) {
        this.text = text;
    }

    public String toString() {
        return text;
    }
}
