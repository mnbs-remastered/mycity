package ua.com.mnbs.mylviv.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ua.com.mnbs.mylviv.R;
import ua.com.mnbs.mylviv.activities.BaseActivity;
import ua.com.mnbs.mylviv.models.User;

public class NotificationsService extends FirebaseMessagingService {

    public static final int NOTIFICATION_KEY = 55;

    public NotificationsService() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage.getData().isEmpty()) {
            System.out.println("Notification is received:\ntitle: " +
                    remoteMessage.getNotification().getTitle() + "\nbody:" + remoteMessage.getNotification().getBody());
            showNotification(remoteMessage.getNotification().getTitle(),
                    remoteMessage.getNotification().getBody());
        } else {
            showNotification(remoteMessage.getData());
        }
    }

    private void showNotification(Map<String, String> data) {
        System.out.println("Notification is received:\ntitle: " +
                data.get("title") + "\nbody:" + data.get("body"));
        showNotification(data.get("title"), data.get("body"));
    }

    private void showNotification(String title, String body) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String CHANNEL_ID = "my_channel_01";
        NotificationCompat.Builder notification;

        Intent intent = new Intent(this, BaseActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("request code", NOTIFICATION_KEY);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, NOTIFICATION_KEY, intent, 0);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, "MyCity", importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setAutoCancel(true)
                .setDefaults(android.app.Notification.DEFAULT_ALL)
                .setContentTitle(title)
                .setContentText(body)
                .setContentIntent(pendingIntent)
                .setContentInfo("info")
                .setSmallIcon(R.mipmap.ic_launcher);
        notificationManager.notify(new Random().nextInt(), notification.build());
    }

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        try {
            FirebaseDatabase.getInstance().getReference("NotificationTokens")
                    .push().setValue(token).addOnSuccessListener(task ->
                    Log.e("Success", "uploading token is successful")
            ).addOnFailureListener(e -> Log.e("Error", e.getMessage()));
            writeNewToken(token);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }
    }

    public void writeNewToken(String token) throws IOException {
        File myFile = new File(this.getFilesDir(), "tokens.txt");
        try (FileOutputStream fos = new FileOutputStream(myFile);
             OutputStreamWriter osw = new OutputStreamWriter(fos);
             BufferedWriter buffReader = new BufferedWriter(osw)) {
            buffReader.write(token + "\n");
        } finally {
            Log.e("Wrote!", token);
        }
    }
}