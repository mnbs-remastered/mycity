package ua.com.mnbs.mylviv.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Source;
import com.google.firebase.storage.StorageReference;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import butterknife.OnClick;
import butterknife.Unbinder;
import ua.com.mnbs.mylviv.R;
import ua.com.mnbs.mylviv.fragments.AboutUsFragment;
import ua.com.mnbs.mylviv.fragments.AccountSettingsFragment;
import ua.com.mnbs.mylviv.fragments.ComplaintStatusFragment;
import ua.com.mnbs.mylviv.fragments.MyComplaintsFragment;
import ua.com.mnbs.mylviv.fragments.NotificationsFragment;
import ua.com.mnbs.mylviv.fragments.SettingsFragment;
import ua.com.mnbs.mylviv.fragments.SolvedFragment;
import ua.com.mnbs.mylviv.fragments.ToComplainFragment;
import ua.com.mnbs.mylviv.fragments.TopUsersFragment;
import ua.com.mnbs.mylviv.managers.FragmentManager;
import ua.com.mnbs.mylviv.managers.ImageSetter;
import ua.com.mnbs.mylviv.managers.InternetChecker;
import ua.com.mnbs.mylviv.managers.KeyboardHider;
import ua.com.mnbs.mylviv.models.User;

import static android.view.View.GONE;

public class BaseActivity extends AppCompatActivity {

    public FirebaseAuth mAuth;
    protected Unbinder unbinder;
    private StorageReference myRef;
    public Toolbar toolbar;
    private CollectionReference firestore;
    ViewPager viewPager;
    MenuItem prevMenuItem;
    NotificationsFragment notificationsFragment;
    MyComplaintsFragment myComplaintsFragment;
    SolvedFragment solvedFragment;
    AboutUsFragment aboutUsFragment;
    public BottomNavigationView navigation;

    @SuppressLint("RestrictedApi")
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.navigation_notifications:
                if (FragmentManager.getCurrentFragment(BaseActivity.this) instanceof NotificationsFragment) {
                    break;
                }
                viewPager.setCurrentItem(0);
                viewPager.setVisibility(View.VISIBLE);
                getSupportFragmentManager().popBackStack();
                checkingFragmentForViewPager();
                setActionBarTitle(R.string.notifications_title);
                break;
            case R.id.navigation_my_complaints:
                if (FragmentManager.getCurrentFragment(BaseActivity.this) instanceof MyComplaintsFragment) {
                    break;
                }
                viewPager.setCurrentItem(1);
                viewPager.setVisibility(View.VISIBLE);
                getSupportFragmentManager().popBackStack();
                checkingFragmentForViewPager();
                setActionBarTitle(R.string.my_complaints_title);
                break;
            case R.id.navigation_solved:
                if (FragmentManager.getCurrentFragment(BaseActivity.this) instanceof SolvedFragment) {
                    break;
                }
                viewPager.setCurrentItem(2);
                viewPager.setVisibility(View.VISIBLE);
                getSupportFragmentManager().popBackStack();
                checkingFragmentForViewPager();
                setActionBarTitle(R.string.solved_complaints_title);
                break;

            case R.id.navigation_other:
                MenuBuilder builder = new MenuBuilder(this);
                MenuInflater inflater = new MenuInflater(this);
                inflater.inflate(R.menu.bottom_menu, builder);
                MenuPopupHelper helper = new MenuPopupHelper(this, builder, findViewById(R.id.navigation_other));
                helper.setForceShowIcon(true);

                builder.setCallback(new MenuBuilder.Callback() {
                    @Override
                    public boolean onMenuItemSelected(MenuBuilder menuBuilder, MenuItem menuItem) {
                        Fragment selectedFragment = null;
                        viewPager = findViewById(R.id.viewpager);
                        switch (menuItem.getItemId()) {
                            case R.id.navigation_about_us:
                                if (FragmentManager.getCurrentFragment(BaseActivity.this) instanceof AboutUsFragment) {
                                    break;
                                }
                                viewPager.setVisibility(GONE);
                                selectedFragment = new AboutUsFragment();
                                break;
                            case R.id.navigation_account:
                                if (FragmentManager.getCurrentFragment(BaseActivity.this) instanceof AccountSettingsFragment) {
                                    break;
                                }
                                viewPager.setVisibility(GONE);
                                selectedFragment = new AccountSettingsFragment();
                                break;
                            case R.id.navigation_settings:
                                if (FragmentManager.getCurrentFragment(BaseActivity.this) instanceof SettingsFragment) {
                                    break;
                                }
                                viewPager.setVisibility(GONE);
                                selectedFragment = new SettingsFragment();
                                break;
                            case R.id.navigation_log_out:
                                viewPager.setVisibility(GONE);
                                LoginManager.getInstance().logOut();
                                SharedPreferences.Editor editor = getSharedPreferences("UserInfo", MODE_PRIVATE).edit();
                                editor.clear();
                                editor.apply();
                                mAuth.signOut();
                                break;
                            case R.id.navigation_top_users:
                                if (FragmentManager.getCurrentFragment(BaseActivity.this) instanceof TopUsersFragment) {
                                    break;
                                }
                                viewPager.setVisibility(GONE);
                                selectedFragment = new TopUsersFragment();
                                break;

                        }
                        if (selectedFragment != null) {
                            FragmentManager.open(selectedFragment, BaseActivity.this);
                        }
                        return false;
                    }

                    @Override
                    public void onMenuModeChange(MenuBuilder menuBuilder) {

                    }
                });

                helper.show();
                break;
        }

        if (fragment != null) {
            FragmentManager.open(fragment, BaseActivity.this);
        }

        KeyboardHider.hideKeyboard(BaseActivity.this);
        return true;
    };

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Intent intent = new Intent(BaseActivity.this, LoginActivity.class);

        FirebaseUser userToCheck = FirebaseAuth.getInstance().getCurrentUser();

        if (userToCheck == null) {
            startActivity(intent);
            return;
        }



        setContentView(R.layout.activity_base_bottom);

        toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        setActionBarTitle(R.string.notifications_title);

        navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        viewPager = findViewById(R.id.viewpager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                } else {
                    navigation.getMenu().getItem(0).setChecked(false);
                }
                Log.d("page", "onPageSelected: " + position);
                Log.d("page", "CurrentFragment: " + getSupportActionBar().getTitle().toString());

                if (position == 2) {
                    navigation.getMenu().getItem(position + 1).setChecked(true);
                    setActionBarTitle(R.string.solved_complaints_title);
                } else if (position == 1) {
                    navigation.getMenu().getItem(position).setChecked(true);
                    setActionBarTitle(R.string.my_complaints_title);
                } else {
                    navigation.getMenu().getItem(position).setChecked(true);
                    setActionBarTitle(R.string.notifications_title);
                }
                prevMenuItem = navigation.getMenu().getItem(position);
                getSupportFragmentManager().popBackStack();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        setupViewPager(viewPager);

        FloatingActionButton fabButton = findViewById(R.id.fab);
        fabButton.setOnClickListener(v -> {
            getSupportFragmentManager().popBackStack();
            viewPager.setVisibility(GONE);
            if (!(FragmentManager.getCurrentFragment(BaseActivity.this) instanceof ToComplainFragment)) {
                FragmentManager.open(new ToComplainFragment(), this);
            }
            navigation.getMenu().findItem(R.id.nothing).setChecked(true);
        });


        Intent intentLocation = getIntent();
        Integer id = intentLocation.getIntExtra("location", 2);
        if (id == 1) {
            FragmentManager.open(new SettingsFragment(), this);
        }

        mAuth = FirebaseAuth.getInstance();

        RelativeLayout bottomLayout = findViewById(R.id.bottom_layout);

        final View activityRootView = findViewById(R.id.base_activity_layout);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
            if (heightDiff > dpToPx(BaseActivity.this, 200)) {
                //TODO: do smth with bottom navigation
            } else {

            }
        });

        ImageView avatar = toolbar.findViewById(R.id.avatar);
        avatar.setOnClickListener(v -> {
            if (!(FragmentManager.getCurrentFragment(BaseActivity.this) instanceof AccountSettingsFragment)) {
                Fragment fragment1 = new AccountSettingsFragment();
                getSupportFragmentManager().popBackStack();
                viewPager.setVisibility(GONE);
                FragmentManager.open(fragment1, this);
                navigation.getMenu().findItem(R.id.navigation_other).setChecked(true);
            }
        });

        SharedPreferences preferences = getSharedPreferences("UserInfo", MODE_PRIVATE);
        String usersPhoto = preferences.getString("uri", "null");
        AtomicBoolean imageIsSet = new AtomicBoolean(false);

        if (!usersPhoto.equals("null")) {
            ImageSetter.forUsers().setImage(usersPhoto, avatar);
            imageIsSet.set(true);
        }

        firestore = FirebaseFirestore.getInstance().collection("UsersList");

        firestore.document(mAuth.getCurrentUser().getUid()).get(Source.SERVER)
                .addOnSuccessListener(documentSnapshot -> {
                    User currentUser = documentSnapshot.toObject(User.class);
                    updateUserNotificationTokens(currentUser);
                    SharedPreferences.Editor editor = preferences.edit();
                    try {
                        editor.putString("uri", currentUser.getImageUrl());
                        editor.apply();
                        if (!imageIsSet.get()) {
                            ImageSetter.forComplaints().setImage(currentUser.getImageUrl(), avatar);
                            imageIsSet.set(true);
                        }
                    } catch (NullPointerException e1) {
                    }
                }).addOnFailureListener(e -> Log.e("TAG", e.getMessage()));

        mAuth.addAuthStateListener(firebaseAuth -> {
            if (firebaseAuth.getCurrentUser() == null) {
                startActivity(intent);
            }
        });
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(Color.parseColor("#757575"));
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

    }

    private void updateUserNotificationTokens(User currentUser) {
        String token = currentUser.getNotificationToken();

        File myFile = new File(this.getFilesDir(), "tokens.txt");
        String tokenToAdd = "";
        try (FileInputStream fos = new FileInputStream(myFile); InputStreamReader osw = new InputStreamReader(fos);
             BufferedReader buffReader = new BufferedReader(osw)) {
            tokenToAdd = buffReader.readLine();
        } catch (IOException e) {
            Log.e("IOException", e.getMessage());
        }

        if (token == null) {
            firestore.document(mAuth.getCurrentUser().getUid()).update("notificationTokens", tokenToAdd);
            return;
        }
        if (!token.equals(tokenToAdd)) {
            if (!tokenToAdd.equals("")) {
                firestore.document(mAuth.getCurrentUser().getUid()).update("notificationTokens", token);
            }
        }
    }

    private boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (FragmentManager.getCurrentFragment(BaseActivity.this) instanceof ComplaintStatusFragment) {
            Fragment fragment = FragmentManager.getCurrentFragment(this);
            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            String dataSolved = prefs.getString("solved", "MyComplaints");
            String dataMy = prefs.getString("My", "noValue");
            if (dataSolved == "SolvedComplaints") {
                viewPager.setCurrentItem(2);
                viewPager.setVisibility(View.VISIBLE);
                setActionBarTitle(R.string.solved_complaints_title);
            } else {
                viewPager.setCurrentItem(2);
                viewPager.setVisibility(View.VISIBLE);
                setActionBarTitle(R.string.solved_complaints_title);
            }
            if (dataMy == "MyComplaints") {
                viewPager.setCurrentItem(1);
                viewPager.setVisibility(View.VISIBLE);
                setActionBarTitle(R.string.my_complaints_title);
            } else {

            }
        } else {

            List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
            int backStackCount = getSupportFragmentManager().getBackStackEntryCount();
            Intent homeIntent = new Intent(Intent.ACTION_MAIN);
            homeIntent.addCategory(Intent.CATEGORY_HOME);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeIntent);

            boolean handled;
            for (Fragment f : fragmentList) {
                if (f instanceof ToComplainFragment) {
                    handled = ((ToComplainFragment) f).onBackPressed();

                    if (handled) {
                        super.onBackPressed();
                        break;
                    }

                    if (!handled) {
                        AlertDialog alertDialog = new AlertDialog.Builder(BaseActivity.this).create();
                        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        LayoutInflater inflater = LayoutInflater.from(BaseActivity.this);
                        View exitDialogView = inflater.inflate(R.layout.dialog, null);
                        alertDialog.setView(exitDialogView);

                        TextView dialogQuestion = exitDialogView.findViewById(R.id.dialog_question);
                        dialogQuestion.setText(R.string.exit_to_complain);

                        exitDialogView.findViewById(R.id.dialog_yes).setOnClickListener(view -> {
                            alertDialog.dismiss();
                            getSupportFragmentManager().popBackStackImmediate();
                            super.onBackPressed();

                            if (InternetChecker.isInternetAvailable()) {
                                SharedPreferences preferences = getSharedPreferences("CachedComplaint", MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("category", "");
                                editor.putString("location", "");
                                editor.putString("title", "");
                                editor.putString("description", "");
                                editor.apply();
                            }
                        });
                        exitDialogView.findViewById(R.id.dialog_no).setOnClickListener(view -> alertDialog.dismiss());
                        alertDialog.show();
                        break;
                    }
                }
                super.onBackPressed();
            }
        }
    }

    public void listenToHideKeyboard(View view) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener((v, event) -> {
                KeyboardHider.hideKeyboard(BaseActivity.this);
                return false;
            });
        }

        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                listenToHideKeyboard(innerView);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        Fragment fragment = null;
        if (item.getItemId() == R.id.about_us) {
            fragment = new AboutUsFragment();
        } else if (item.getItemId() == R.id.account_settings) {
            fragment = new AccountSettingsFragment();
        }
        if (fragment != null) {
            FragmentManager.open(fragment, this);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void finish() {

    }

    public void setActionBarTitle(int title) {
        getSupportActionBar();
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText(title);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        notificationsFragment = new NotificationsFragment();
        myComplaintsFragment = new MyComplaintsFragment();
        solvedFragment = new SolvedFragment();
        viewPagerAdapter.addFragment(notificationsFragment);
        viewPagerAdapter.addFragment(myComplaintsFragment);
        viewPagerAdapter.addFragment(solvedFragment);
        viewPager.setAdapter(viewPagerAdapter);
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();

        public ViewPagerAdapter(android.support.v4.app.FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }
    }

    public void checkingFragmentForViewPager() {
        if (FragmentManager.getCurrentFragment(BaseActivity.this) instanceof AccountSettingsFragment) {
            removeCurrentFragment();
        } else if (FragmentManager.getCurrentFragment(BaseActivity.this) instanceof TopUsersFragment) {
            removeCurrentFragment();
        } else if (FragmentManager.getCurrentFragment(BaseActivity.this) instanceof SettingsFragment) {
            removeCurrentFragment();
        } else if (FragmentManager.getCurrentFragment(BaseActivity.this) instanceof AboutUsFragment) {
            removeCurrentFragment();
        } else if (FragmentManager.getCurrentFragment(BaseActivity.this) instanceof ToComplainFragment) {
            removeCurrentFragment();
        } else if (FragmentManager.getCurrentFragment(BaseActivity.this) instanceof ComplaintStatusFragment) {
            removeCurrentFragment();
        } else {

        }
    }

    public void removeCurrentFragment() {
        Fragment fragment = FragmentManager.getCurrentFragment(this);
        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
    }

    public static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }
}
