package ua.com.mnbs.mylviv.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import ua.com.mnbs.mylviv.R;
import ua.com.mnbs.mylviv.activities.BaseActivity;
import ua.com.mnbs.mylviv.managers.InternetChecker;
import ua.com.mnbs.mylviv.managers.KeyboardHider;
import ua.com.mnbs.mylviv.managers.PathGetter;
import ua.com.mnbs.mylviv.managers.Transliterator;
import ua.com.mnbs.mylviv.models.Complaint;
import ua.com.mnbs.mylviv.models.ComplaintType;
import ua.com.mnbs.mylviv.models.Status;
import ua.com.mnbs.mylviv.models.User;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ToComplainFragment extends Fragment {

    public static final int NO_GEOLOCATION_PROVIDED = -1000000;
    public static final String TAG = "ToCOMPLAIN";
    private Unbinder unbinder;
    private User user;
    private static Activity mContext;

    public static final int GALLERY_REQUEST = 1846;
    public static final int CAMERA_REQUEST = 45655;
    public static final int GEO_PERMISSION_GIVEN = 1;
    public static final int STORAGE_PERMISSION_GIVEN = 15;
    public static final int MINIMAL_TEXT_LENGHT = 4;

    private boolean imageISChosen = false;
    private Date currentDate = Calendar.getInstance().getTime();
    private Uri imageUri;

    private Address address;

    private float[] coordinates = new float[2];

    @BindView(R.id.submit_problem_description)
    EditText descriptionEditText;
    @BindView(R.id.submit_problem_location)
    EditText locationEditText;
    @BindView(R.id.submit_problem_category)
    Spinner categorySpinner;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.toComplainLayout)
    ScrollView scrollView;
    @BindView(R.id.submit_problem_photo)
    ImageView problemImageView;
    @BindView(R.id.problem_title)
    EditText titleEditText;
    @BindView(R.id.submit_problem_button)
    FloatingActionButton submitProblem;

    Location location = new Location("");

    private Bitmap selectedImage;
    ByteArrayInputStream finalCompressedImage;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((BaseActivity) getActivity())
                .setActionBarTitle(R.string.to_complain_title);
        return inflater.inflate(R.layout.fragment_to_complain, null);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        unbinder = ButterKnife.bind(this, view);

        SharedPreferences preferences = getActivity().getSharedPreferences(
                "CachedComplaint", Context.MODE_PRIVATE);
        locationEditText.setText(preferences.getString("location", ""));
        titleEditText.setText(preferences.getString("title", ""));
        descriptionEditText.setText(preferences.getString("description", ""));
        setSpinnerCategory(preferences.getString("category", ""));
        //TODO: add image caching

        submitProblem.setEnabled(false);

        final ArrayAdapter<ComplaintType> sexSpinnerArrayAdapter = new ArrayAdapter<ComplaintType>(
                getApplicationContext(), R.layout.spinner_item, ComplaintType.values()) {
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position % 2 == 1) {
                    tv.setBackgroundColor(Color.parseColor("#FFFFFF"));
                } else {
                    tv.setBackgroundColor(Color.parseColor("#FFFFFF"));
                }
                if (position == 0) {
                    tv.setBackgroundColor(Color.parseColor("#E1E1E1"));
                }
                return view;
            }
        };

        sexSpinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        categorySpinner.setAdapter(sexSpinnerArrayAdapter);

        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });
        
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("RestrictedApi")
    @OnTextChanged(R.id.submit_problem_description)
    public void getDescription() {
        try {
            String category = categorySpinner.getSelectedItem().toString().trim();
            String location = locationEditText.getText().toString();
            String description = descriptionEditText.getText().toString();

            if (checkRequirements(category, location, description)) {
                submitProblem.setBackgroundTintList(getContext().getColorStateList(R.color.colorPrimary));
                submitProblem.setEnabled(true);
            }
        } catch (NullPointerException e) {
            Log.e("ERROR", e.getMessage());
        }
    }

    @OnClick(R.id.submit_problem_get_location)
    public void getLocation() throws SecurityException {

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};
            requestPermissions(permissions, GEO_PERMISSION_GIVEN);
        } else {
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(
                        location.getLatitude(), location.getLongitude(), 1);
                address = addresses.get(0);
                String outputText = address.getAddressLine(0);
                locationEditText.setText(outputText);
            } catch (IOException e) {
                Log.d(TAG, "No GPS provided");
            } catch (NullPointerException e) {
                Log.d(TAG, "No location available");
                Toast.makeText(getContext(), "Локація недоступна", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) throws SecurityException {
        switch (requestCode) {
            case GEO_PERMISSION_GIVEN: {
                LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                try {
                    List<Address> addresses = geocoder.getFromLocation(
                            location.getLatitude(), location.getLongitude(), 1);
                    address = addresses.get(0);
                    String outputText = address.getAddressLine(0);
                    locationEditText.setText(outputText);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
            case STORAGE_PERMISSION_GIVEN: {
                AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                LayoutInflater inflater = LayoutInflater.from(getContext());
                View deleteDialogView = inflater.inflate(R.layout.dialog_for_choosing_image, null);
                alertDialog.setView(deleteDialogView);

                TextView dialogChoosing = deleteDialogView.findViewById(R.id.dialog_choosing);
                dialogChoosing.setText(R.string.choose_the_way_to_add_image);

                deleteDialogView.findViewById(R.id.dialog_camera).setOnClickListener(v -> {
                    takePicture();
                    alertDialog.dismiss();
                });
                deleteDialogView.findViewById(R.id.dialog_gallery).setOnClickListener(v -> {
                    choosePhotoGallery();
                    alertDialog.dismiss();
                });
                alertDialog.show();
                break;
            }
            default:
                return;
        }
    }

    @OnClick(R.id.submit_problem_button)
    public void getInputComplaint() {
        String category = categorySpinner.getSelectedItem().toString().trim();
        String location = locationEditText.getText().toString();
        String title = titleEditText.getText().toString();
        String description = descriptionEditText.getText().toString();

        if (!InternetChecker.isInternetAvailable()) {
            Toast.makeText(getActivity(), "Підключіться до Інтернету", Toast.LENGTH_LONG).show();
            Log.i(TAG, "No Internet provided");

            SharedPreferences preferences = getActivity()
                    .getSharedPreferences("CachedComplaint", getActivity().MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("category", category);
            editor.putString("location", location);
            editor.putString("title", title);
            editor.putString("description", description);
            editor.apply();

        } else if (checkRequirements(category, location, description)) {
            progressBar.setVisibility(ProgressBar.VISIBLE);
            scrollView.setVisibility(ScrollView.INVISIBLE);

            SharedPreferences preferences = getActivity()
                    .getSharedPreferences("CachedComplaint", getActivity().MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("category", "");
            editor.putString("location", "");
            editor.putString("title", "");
            editor.putString("description", "");
            editor.commit();

            FirebaseFirestore.getInstance().collection("UsersList").document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .get().addOnSuccessListener(documentSnapshot ->
                submitToDatabase(new Complaint(category, location, title, description, currentDate,
                        null), documentSnapshot.toObject(User.class))
            );

        }
        KeyboardHider.hideKeyboard(getActivity());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK)
            try {
                switch (requestCode) {
                    case CAMERA_REQUEST:
                        Bundle extras = data.getExtras();
                        Bitmap imageBitmap = (Bitmap) extras.get("data");
                        processImage(imageBitmap);
                        break;
                    case GALLERY_REQUEST:
                        imageUri = data.getData();
                        processImage(imageUri);
                        String realPath = PathGetter.getPath(getActivity(), imageUri);
                        if (Build.VERSION.SDK_INT >= 24) {
                            try {
                                ExifInterface exifInterface = new ExifInterface(realPath);
                                exifInterface.getLatLong(coordinates);
                                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                                try {
                                    location.setLatitude(coordinates[0]);
                                    location.setLongitude(coordinates[1]);
                                    List<Address> addresses = geocoder.getFromLocation(
                                            location.getLatitude(), location.getLongitude(), 1);
                                    address = addresses.get(0);
                                    String outputText = address.getAddressLine(0);
                                    locationEditText.setText(outputText);
                                } catch (NullPointerException | IndexOutOfBoundsException e) {
                                    Log.i(TAG, "No GPS provided");
                                }
                            } catch (IOException e) {
                                Log.i(TAG,"Nothing provided");
                            } catch (Exception e) {
                                Log.e(TAG, e.getMessage());
                            }
                        } else {
                            Log.i(TAG, "No image is picked");
                        }
                }
            } catch (IOException e1) {
                e1.printStackTrace();
                Log.e(TAG, "Something went wrong: " + e1.getMessage());
            }
    }

    private void processImage(Bitmap image) throws IOException {
        float height = image.getHeight();
        float width = image.getWidth();

        float proportion = width / height;

        try {
            if (proportion > 1) {
                selectedImage = Bitmap.createScaledBitmap(image, Math.round(400 * proportion), 400, false);
            } else {
                selectedImage = Bitmap.createScaledBitmap(image, 400, Math.round(400 / proportion), false);
            }
        } catch (NullPointerException e) {
            selectedImage = image;
            image.recycle();
        }
        problemImageView.setImageBitmap(selectedImage);
        imageISChosen = true;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        byte[] bitmapdata = bos.toByteArray();
        finalCompressedImage = new ByteArrayInputStream(bitmapdata);
        bos.close();
    }

    private void processImage(Uri imageUri) throws IOException {
        final InputStream imageStream = getActivity().getContentResolver().openInputStream(imageUri);
        final Bitmap image = BitmapFactory.decodeStream(imageStream);
        imageStream.close();
        ExifInterface exif = new ExifInterface(PathGetter.getPath(getActivity(), imageUri));
        int rotation = exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        int rotationInDegrees = convertToDegrees(rotation);
        Matrix matrix = new Matrix();
        if (rotation != 0) {
            matrix.preRotate(rotationInDegrees);
        }

        Bitmap selectedImageBeforeScaling = Bitmap.createBitmap(image, 0, 0, image.getWidth(),
                image.getHeight(), matrix, true);
        processImage(selectedImageBeforeScaling);
    }

    private int convertToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    private void submitToDatabase(Complaint complaint, User user) {
        DocumentReference firestore = FirebaseFirestore.getInstance().collection("Cities").document(Transliterator.transiterate(user.getCity())).collection("Complaints").document();
        StorageReference storage = FirebaseStorage.getInstance().getReference().child("photos/complaints/" +
                FirebaseAuth.getInstance().getCurrentUser().getUid() + System.currentTimeMillis());

        Log.i("d", "starting uploading image");
        storage.putStream(finalCompressedImage)
                .addOnSuccessListener(taskSnapshot -> {
                    Log.i("d", "image uploaded");
                    storage.getDownloadUrl().addOnSuccessListener(url -> {
                        Complaint upload = new Complaint(complaint.getType(), complaint.getAddress(),
                                complaint.getTitle(), complaint.getDescription(), complaint.getDate(),
                                url.toString());
                        upload.setStatus(Status.SENT.toString());
                        upload.setUser(FirebaseAuth.getInstance().getCurrentUser().getUid());
                        Log.i("UploadUrl", url.toString());
                        firestore.set(upload).addOnFailureListener(e -> {
                            Log.e(TAG, "Upload failed: " + e.getLocalizedMessage());
                            storage.delete();
                        }).addOnSuccessListener(snapshot ->
                            addProblems(firestore.getId())
                        );
                        Log.i(TAG, "Complaint uploaded successfully");
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container, new MyComplaintsFragment()).addToBackStack(null).commit();
                    });
                }).addOnFailureListener(e -> Log.e(TAG, e.getMessage()));
    }

    private boolean checkRequirements(String description, String category, String location) {
        if (!imageISChosen) {
            return false;

        } else if (category.equals("Обрати тип проблеми")) {
            categorySpinner.setBackgroundColor(Color.YELLOW);
            return false;

        } else if (location.length() < MINIMAL_TEXT_LENGHT) {
            locationEditText.setHintTextColor(Color.RED);
            return false;

        } else if (description.length() < MINIMAL_TEXT_LENGHT) {
            descriptionEditText.setHintTextColor(Color.RED);
            return false;
        } else if (titleEditText.getText().toString().equals("")) {
            titleEditText.setHintTextColor(Color.RED);
            return false;
        }
        return true;
    }

    private void addProblems(String id) {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FirebaseFirestore.getInstance()
                .collection("UsersList")
                .document(uid)
                .get().addOnSuccessListener(documentSnapshot -> {
            user = documentSnapshot.toObject(User.class);

            int solvedProblemsNumber = user.getSolvedProblemsNumber();
            solvedProblemsNumber++;

            ArrayList<String> complaintsIds = user.getComplaintsIds();
            complaintsIds.add(id);

            HashMap<String, Object> update = new HashMap<>();
            update.put("solvedProblemsNumber", solvedProblemsNumber);
            update.put("complaintsIds", complaintsIds);

            FirebaseFirestore.getInstance()
                    .collection("UsersList")
                    .document(uid)
                    .update(update);
        });
    }

    @OnClick(R.id.submit_problem_photo)
    public void choosePhotoFromGallery() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
            requestPermissions(permissions, STORAGE_PERMISSION_GIVEN);
        } else {
            AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
            alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View deleteDialogView = inflater.inflate(R.layout.dialog_for_choosing_image, null);
            alertDialog.setView(deleteDialogView);

            TextView dialogChoosing = deleteDialogView.findViewById(R.id.dialog_choosing);
            dialogChoosing.setText(R.string.choose_the_way_to_add_image);

            deleteDialogView.findViewById(R.id.dialog_camera).setOnClickListener(v -> {
                takePicture();
                alertDialog.dismiss();
            });
            deleteDialogView.findViewById(R.id.dialog_gallery).setOnClickListener(v -> {
                choosePhotoGallery();
                alertDialog.dismiss();
            });
            alertDialog.show();
        }
    }

    private void setSpinnerCategory(String category) {
        if (category != null) {
            if (category.equals(ComplaintType.LIGHT.toString())) {
                categorySpinner.setSelection(1);
            } else if (category.equals(ComplaintType.VANDALISM.toString())) {
                categorySpinner.setSelection(2);
            } else if (category.equals(ComplaintType.ROAD.toString())) {
                categorySpinner.setSelection(3);
            } else if (category.equals(ComplaintType.ANIMAL.toString())) {
                categorySpinner.setSelection(4);
            } else if (category.equals(ComplaintType.COMMUNAL_SERVICES.toString())) {
                categorySpinner.setSelection(5);
            } else if (category.equals(ComplaintType.OTHER.toString())) {
                categorySpinner.setSelection(6);
            }
        }
    }

    public void choosePhotoGallery() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, GALLERY_REQUEST);
    }

    public void takePicture() {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePicture, CAMERA_REQUEST);
    }

    public boolean onBackPressed() {
        String category = categorySpinner.getSelectedItem().toString().trim();
        String location = locationEditText.getText().toString();
        String title = titleEditText.getText().toString();
        String description = descriptionEditText.getText().toString();
        return !isSomethingWritten(category, location, title, description);
    }

    private boolean isSomethingWritten(String category, String location, String title, String description) {
        return imageISChosen || !location.equals("") || !title.equals("") || !description.equals("")
                || !category.equals("Обрати тип проблеми");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
