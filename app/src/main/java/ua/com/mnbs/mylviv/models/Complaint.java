package ua.com.mnbs.mylviv.models;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class Complaint implements Serializable {

    private String type;
    private String address;
    private String status;
    private String title;
    private int pictureId;
    private String company;
    private String description;
    private Date date;
    private String imageUrl;
    private String user;
    private Date deadline;
    private String feedback;


    //full constructor
    public Complaint(ComplaintType type, String address, Status status, String title,
                     int pictureId, String company, Date date, String description) {
        this.type = type.toString();
        this.address = address;
        this.status = status.toString();
        this.title = title;
        this.pictureId = pictureId;
        this.company = company;
        this.date = date;
        this.description = description;
    }


    //temp constructor
    public Complaint(String type, String address, String title,
                     String description, Date date, String imageUrl) {
        this.type = type;
        this.address = address;
        this.title = title;
        this.description = description;
        this.date = date;
        this.imageUrl = imageUrl;
    }

    //for СomplaintStatusActivity
    public Complaint(ComplaintType type, String address, Status status, String title,
                     int pictureId, String company, String description) {
        this(type, address, status, title, pictureId, company, Calendar.getInstance().getTime(), description);
    }

    public Complaint(ComplaintType type, String address, Status status, String title, int pictureId) {
        this(type, address, status, title, pictureId, "", "");
    }

    public Complaint(ComplaintType type, String address, String title, int pictureId) {
        this(type, address, Status.NONE, title, pictureId);
    }

    public Complaint() {
    }

    public int getPictureId() {
        return this.pictureId;
    }

    public void setPictureId(int pictureId) {
        this.pictureId = pictureId;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCompany() {
        return this.company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
}