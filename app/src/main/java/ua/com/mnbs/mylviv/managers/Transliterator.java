package ua.com.mnbs.mylviv.managers;

public class Transliterator {
    public static String transiterate(String ukr) {
        String[] ukrainian = new String[]{"Львів", "Дніпро", "Київ"};
        String[] english = new String[]{"Lviv", "Dnipro", "Kyiv"};
        for (int i = 0; i< 3; i++) {
            if (ukr.equals(ukrainian[i])) {
                return english[i];
            }
        }
        return ukr;
    }
}
