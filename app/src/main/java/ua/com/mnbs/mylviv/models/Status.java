package ua.com.mnbs.mylviv.models;

public enum Status {

    REJECTED("Відхилено"),
    SENT("Надіслано"),
    ACCEPTED("Прийнято"),
    IN_PROGRESS("Вирішується"),
    SOLVED("Вирішено"),
    NONE("");

    private String text;

    Status(String text) {
        this.text = text;
    }

    public String toString() {
        return text;
    }
}
