package ua.com.mnbs.mylviv.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.ArraySwipeAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.com.mnbs.mylviv.R;


public class PlaceAdapter extends ArraySwipeAdapter<String> {

    private static Activity mContext;
    private static CollectionReference myRef = FirebaseFirestore.getInstance().collection("UsersList");
    private static String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
    private static ArrayList<String> places;

    public PlaceAdapter(Activity context, ArrayList<String> placesList) {
        super(context, 0, placesList);
        mContext = context;
    }

    public static void setPlaces(ArrayList<String> places) {
        PlaceAdapter.places = places;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PlaceAdapter.ViewHolder holder;

        if (convertView != null) {
            holder = (PlaceAdapter.ViewHolder) convertView.getTag();
        } else {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.place_list_item, parent, false);
            holder = new PlaceAdapter.ViewHolder(convertView);
            convertView.setTag(holder);
        }

        String currentPlace = (String) getItem(position);

        holder.placeTextView.setText(currentPlace);

        holder.swipeLayout.addDrag(SwipeLayout.DragEdge.Left, null);
        holder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {

                holder.deletePlace();
            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onClose(SwipeLayout layout) {

            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
            }
        });

        return convertView;


    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    static class ViewHolder {
        @BindView(R.id.chosen_place) TextView placeTextView;
        @BindView(R.id.delete) View deleteView;
        @BindView(R.id.place_list_item_current_place) ConstraintLayout currentPlaceView;
        @BindView(R.id.swipe) SwipeLayout swipeLayout;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        public void deletePlace() {
            AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
            alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            LayoutInflater inflater = LayoutInflater.from(mContext);
            View deleteDialogView = inflater.inflate(R.layout.dialog, null);
            alertDialog.setView(deleteDialogView);

            TextView dialogQuestion = deleteDialogView.findViewById(R.id.dialog_question);
            dialogQuestion.setText(R.string.delete_place);

            deleteDialogView.findViewById(R.id.dialog_yes).setOnClickListener(v -> {

                places.remove(placeTextView.getText().toString());
                HashMap<String, Object> update = new HashMap<>();
                update.put("addresses", places);
                myRef.document(uid).update(update).addOnSuccessListener(aVoid -> currentPlaceView.removeAllViews());
                alertDialog.dismiss();
            });
            deleteDialogView.findViewById(R.id.dialog_no).setOnClickListener(v -> {
                alertDialog.dismiss();
            });
            alertDialog.show();
            swipeLayout.close();
        }
    }
}
