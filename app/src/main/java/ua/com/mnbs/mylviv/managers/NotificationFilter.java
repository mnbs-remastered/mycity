package ua.com.mnbs.mylviv.managers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import ua.com.mnbs.mylviv.models.Notification;
import ua.com.mnbs.mylviv.models.User;

public class NotificationFilter {

    public static List<Notification> filterNotifications(List<Notification> oldNotifications, User user) {
        ArrayList<Notification> newNotifications = new ArrayList<>();
        if (user.getSettings() == null | user.getAddresses() == null) {
            return oldNotifications;
        }
        for (Notification notification : oldNotifications) {
            if (checkNotification(notification, user))
                newNotifications.add(notification);
        }
        return newNotifications;
    }

    public static boolean checkNotification(Notification notification, User user) {
        if (user.getSettings() == null | user.getAddresses() == null) {
            return false;
        }
        boolean isValid = true;
        for (String address : user.getAddresses()) {
            if (getExactAddress(address.toLowerCase()).equals(getExactAddress(notification.getStreet().toLowerCase()))) {
                isValid = true;
                break;
            } else {
                isValid = false;
            }
        }
        try {
            if (!user.getSettings().get(notification.getType()))
                return false;
        } catch (NullPointerException e) {
            return false;
        }
        return isValid;
    }

    private static String getExactAddress(String rawAddress) {
        Pattern prospect = Pattern.compile("п(роспект|р.|.)");
        String exactAddress = prospect.matcher(rawAddress).replaceAll("проспект");
        Pattern street = Pattern.compile("вул(иця|.)");
        exactAddress = street.matcher(exactAddress).replaceAll("вулиця");
        Pattern city = Pattern.compile(", .*");
        exactAddress = city.matcher(exactAddress).replaceAll("");
        return exactAddress;
    }
}
