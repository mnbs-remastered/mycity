package ua.com.mnbs.mylviv.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.com.mnbs.mylviv.R;

public class ResetPasswordActivity extends AppCompatActivity {

    @BindView(R.id.email_address_where_to_send)
    EditText emailAddress;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.send_email)
    public void sendEmail() {
        String email;
        try {
            email = emailAddress.getText().toString();
        } catch (IllegalArgumentException e) {
            Toast.makeText(this, "Введіть email", Toast.LENGTH_LONG).show();
            return;
        }
        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        progressBar.setVisibility(View.VISIBLE);

        mAuth.setLanguageCode("uk");
        mAuth.sendPasswordResetEmail(email).addOnSuccessListener(
                task -> {
                    Toast.makeText(this, "Лист надіслано", Toast.LENGTH_LONG).show();
                    progressBar.setVisibility(View.GONE);
                    Intent goToLogin = new Intent(ResetPasswordActivity.this, LoginActivity.class);
                    startActivity(goToLogin);
                }
        );
    }
}
