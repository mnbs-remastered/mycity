package ua.com.mnbs.mylviv.models;

public class Settings {

    private String problem;
    private int problemId;
    private boolean checked = false;
    private int problemIco;

    public Settings(String problem, int problemIco){
        this.problem = problem;
        this.problemIco = problemIco;
    }

    public Settings(int problemId, int problemIco) {
        this.problemId = problemId;
        this.problemIco = problemIco;
    }

    public String getProblem() {return problem;}
    public boolean isChecked() {return  checked;}
    public void setChecked(boolean checked) {this.checked = checked;}
    public int getProblemIco() {return  problemIco;}

    public int getProblemId() {
        return problemId;
    }

    public void setProblemId(int problemId) {
        this.problemId = problemId;
    }
}
