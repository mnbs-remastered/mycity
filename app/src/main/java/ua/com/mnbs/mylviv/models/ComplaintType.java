package ua.com.mnbs.mylviv.models;

public enum ComplaintType {

    NOTYPE("Обрати тип проблеми"),
    LIGHT("Освітлення"),
    VANDALISM("Вандалізм"),
    ROAD("Дорога"),
    ANIMAL ("Тварини"),
    COMMUNAL_SERVICES("Комунальні послуги"),
    OTHER("Інше");

    private String text;

    ComplaintType(String text) {
        this.text = text;
    }

    public String toString() {
        return text;
    }
}
