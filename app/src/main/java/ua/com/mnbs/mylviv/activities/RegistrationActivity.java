package ua.com.mnbs.mylviv.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.com.mnbs.mylviv.R;
import ua.com.mnbs.mylviv.models.City;
import ua.com.mnbs.mylviv.models.User;

public class RegistrationActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private User user;

    @BindView(R.id.email) EditText regEmail;
    @BindView(R.id.password_registration) EditText regPassword;
    @BindView(R.id.password_repeat_registration) EditText regRepeatedPassword;
    @BindView(R.id.progressBar) ProgressBar progressBar;
    @BindView(R.id.activity_registration) LinearLayout linearLayout;
    @BindView(R.id.phone) EditText regPhone;
    @BindView(R.id.name) EditText regName;
    @BindView(R.id.birthday) EditText birthdayEditText;
    @BindView(R.id.sex) Spinner sexSpinner;
    @BindView(R.id.spinner_city) Spinner citySpinner;


    private int mYear;
    private int mMonth;
    private int mDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        FirebaseApp.initializeApp(RegistrationActivity.this);
        mAuth = FirebaseAuth.getInstance();

        ButterKnife.bind(this);

        String[] sex = new String[]{
                "Обрати стать",
                "Чоловіча",
                "Жіноча"
        };

        final ArrayList<String> sexList = new ArrayList<>(Arrays.asList(sex));

        final ArrayAdapter<String> sexSpinnerArrayAdapter = new ArrayAdapter<String>(
                getApplicationContext(), R.layout.spinner_item, sexList) {
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position % 2 == 1) {
                    tv.setBackgroundColor(Color.parseColor("#FFFFFF"));
                } else {
                    tv.setBackgroundColor(Color.parseColor("#FFFFFF"));
                }
                if (position == 0) {
                    tv.setBackgroundColor(Color.parseColor("#E1E1E1"));
                }
                return view;
            }
        };

        sexSpinner.setPrompt("Оберіть стать");

        sexSpinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        sexSpinner.setAdapter(sexSpinnerArrayAdapter);

        sexSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });

        final ArrayAdapter<City> citiesSpinnerArrayAdapter = new ArrayAdapter<City>(
                getApplicationContext(), R.layout.spinner_item, City.values()) {
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position % 2 == 1) {
                    tv.setBackgroundColor(Color.parseColor("#FFFFFF"));
                } else {
                    tv.setBackgroundColor(Color.parseColor("#FFFFFF"));
                }
                if (position == 0) {
                    tv.setBackgroundColor(Color.parseColor("#E1E1E1"));
                }
                return view;
            }
        };

        citySpinner.setPrompt("Оберіть місто");

        citiesSpinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        citySpinner.setAdapter(citiesSpinnerArrayAdapter);

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });
    }

    @OnClick(R.id.btn_registration)
    public void register() {
        String email = regEmail.getText().toString();
        String password = regPassword.getText().toString();
        String repeatedPassword = regRepeatedPassword.getText().toString();
        String phoneNumber = regPhone.getText().toString();
        String name = regName.getText().toString();
        String sex = sexSpinner.getSelectedItem().toString().trim();
        String birthday = birthdayEditText.getText().toString();
        String city = citySpinner.getSelectedItem().toString().trim();

        if (!password.equals(repeatedPassword)) {
            Toast.makeText(this, "Паролі не збігаються", Toast.LENGTH_LONG).show();
            return;
        }

        if (!email.equals("") && !password.equals("") && !name.equals("") && !phoneNumber.equals("")) {
                progressBar.setVisibility(ProgressBar.VISIBLE);
                linearLayout.setVisibility(RelativeLayout.INVISIBLE);
                registration(email, password, name, phoneNumber, birthday, sex, city);
        } else if (email.equals("")) {
            Toast.makeText(this, R.string.email_field_required,Toast.LENGTH_LONG).show();
            progressBar.setVisibility(ProgressBar.INVISIBLE);
            linearLayout.setVisibility(RelativeLayout.VISIBLE);
        } else if (password.equals("")) {
            Toast.makeText(this, R.string.password_field_required,Toast.LENGTH_LONG).show();
            progressBar.setVisibility(ProgressBar.INVISIBLE);
            linearLayout.setVisibility(RelativeLayout.VISIBLE);
        } else if (name.equals("")) {
            Toast.makeText(this, "Введіть ім'я, щоб продовжити", Toast.LENGTH_LONG).show();
            progressBar.setVisibility(ProgressBar.INVISIBLE);
            linearLayout.setVisibility(RelativeLayout.VISIBLE);
        } else if (phoneNumber.equals("")) {
            Toast.makeText(this, "Введіть номер телефону, щоб продовжити", Toast.LENGTH_LONG).show();
            progressBar.setVisibility(ProgressBar.INVISIBLE);
            linearLayout.setVisibility(RelativeLayout.VISIBLE);
        } else if (sex.equals("Обрати стать")) {
            Toast.makeText(this, "Оберіть стать, щоб продовжити", Toast.LENGTH_LONG).show();
            progressBar.setVisibility(ProgressBar.INVISIBLE);
            linearLayout.setVisibility(RelativeLayout.VISIBLE);
        }
    }

    @OnClick({R.id.birthday, R.id.calendar_icon})
    public void getBirthday() {
        AlertDialog alertDialog = new AlertDialog.Builder(RegistrationActivity.this).create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        LayoutInflater inflater = LayoutInflater.from(RegistrationActivity.this);
        View calendarDialogView = inflater.inflate(R.layout.dialog_calendar, null);
        alertDialog.setView(calendarDialogView);
        Calendar calendarMin, calendarMax;
        calendarMin = Calendar.getInstance();
        calendarMax = Calendar.getInstance();
        calendarMin.add(Calendar.YEAR, -80);
        calendarMax.add(Calendar.YEAR, -18);
        DatePicker datePicker = calendarDialogView.findViewById(R.id.datePicker);
        datePicker.setMinDate(calendarMin.getTimeInMillis());
        datePicker.setMaxDate(calendarMax.getTimeInMillis());
        calendarDialogView.findViewById(R.id.dialog_save).setOnClickListener(v -> {
            int year = datePicker.getYear();
            int monthOfYear = datePicker.getMonth();
            int dayOfMonth = datePicker.getDayOfMonth();

            birthdayEditText.setText(((dayOfMonth >= 10) ? "" : "0") + dayOfMonth + "/"
                    + ((monthOfYear >= 9) ? "" : "0") + (monthOfYear + 1) + "/" + year);
            alertDialog.dismiss();
        });
        calendarDialogView.findViewById(R.id.dialog_dismiss).setOnClickListener(v -> {
            alertDialog.dismiss();
        });
        alertDialog.show();
    }

    private void registration (final String email, final String password,
                               final String name, final String phoneNumber,
                               final String birthday, final String sex,
                               final String city) {
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, task -> {
            if(task.isSuccessful())  {
                UserProfileChangeRequest profileChangeRequest = new UserProfileChangeRequest.Builder()
                        .setDisplayName(name).build();
                mAuth.getCurrentUser().updateProfile(profileChangeRequest)
                        .addOnCompleteListener(task1 -> {
                            User user = new User(name, email, null, 0);
                            user.setId(mAuth.getCurrentUser().getUid());
                            user.setBirthdayFromStr(birthday);
                            user.setSex(sex);
                            user.setCity(city);
                            user.setPhoneNumber(phoneNumber);

                            HashMap<String, Boolean> settings = new HashMap<>();
                            settings.put(getString(R.string.water_cut_off),true);
                            settings.put(getString(R.string.gas_cut_off),true);
                            settings.put(getString(R.string.electricity_cut_off),true);
                            settings.put(getString(R.string.road_problems),true);
                            settings.put(getString(R.string.transport_problems),true);
                            settings.put(getString(R.string.important_events),true);

                            user.setSettings(settings);

                            SharedPreferences preferences = getSharedPreferences("UserInfo", MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString("name", name);
                            editor.putString("email", email);
                            editor.putString("sex", sex);
                            editor.putString("birthday", birthday);
                            editor.putString("city", city);
                            editor.putString("phoneNumber", phoneNumber);
                            editor.apply();

                            FirebaseFirestore.getInstance().collection("UsersList")
                                    .document(mAuth.getCurrentUser().getUid()).set(user)
                                    .addOnSuccessListener(task2 -> {
                                Intent intent = new Intent(RegistrationActivity.this, BaseActivity.class);
                                startActivity(intent);
                                progressBar.setVisibility(ProgressBar.VISIBLE);
                            }).addOnFailureListener(e -> mAuth.getCurrentUser().delete());
                        });
            } else {
                Toast.makeText(RegistrationActivity.this, getRegistrationErrorMessage(task), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(ProgressBar.INVISIBLE);
                linearLayout.setVisibility(RelativeLayout.VISIBLE);
            }
        });


    }

    private String getRegistrationErrorMessage(Task task) {
        switch (task.getException().getMessage()) {
            case("The email address is already in use by another account"):
                return "Ця email адреса вже зайнята";
            case ("The given password is invalid. [ Password should be at least 6 characters ]") :
                return "Пароль мусить містити більше шести символів";
            case ("The email address is badly formatted") :
                return "Неправильний формат email адреси";
        }
        return "Помилка з'єднання із сервером";
    }
}
