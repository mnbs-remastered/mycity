package ua.com.mnbs.mylviv.managers;

import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import ua.com.mnbs.mylviv.R;

public class ImageSetter {

    private int defaultImage;

    private ImageSetter() {}

    public static ImageSetter forUsers() {
        ImageSetter imageSetter = new ImageSetter();
        imageSetter.defaultImage = R.drawable.user;
        return imageSetter;
    }

    public static ImageSetter forComplaints() {
        ImageSetter imageSetter = new ImageSetter();
        imageSetter.defaultImage = R.drawable.error;
        return imageSetter;
    }

    public void setImage(String url, ImageView imageView, ProgressBar progressBar) {
        Picasso.get()
                .load(url)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .fit()
                .centerCrop()
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        if (progressBar != null)
                            progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception e) {
                        Picasso.get()
                                .load(url)
                                .fit()
                                .centerCrop()
                                .into(imageView, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        if (progressBar != null)
                                            progressBar.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onError(Exception e) {
                                        Log.e("Picasso", e.getMessage(), e);
                                        Picasso.get().load(defaultImage).into(imageView);
                                        if (progressBar != null)
                                            progressBar.setVisibility(View.GONE);
                                    }
                                });
                    }
                });
    }

    public void setImage(String url, ImageView imageView) {
        setImage(url, imageView, null);
    }

    public void setImage(Uri url, ImageView imageView) {
        setImage(url.toString(), imageView);
    }
}