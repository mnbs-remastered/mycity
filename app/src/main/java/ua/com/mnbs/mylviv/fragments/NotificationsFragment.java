package ua.com.mnbs.mylviv.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ua.com.mnbs.mylviv.R;
import ua.com.mnbs.mylviv.adapters.NotificationsAdapter;
import ua.com.mnbs.mylviv.managers.NotificationFilter;
import ua.com.mnbs.mylviv.managers.Transliterator;
import ua.com.mnbs.mylviv.models.Notification;
import ua.com.mnbs.mylviv.models.User;

public class NotificationsFragment extends Fragment {

    @BindView(R.id.notifications_list)
    RecyclerView recyclerView;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;
    @BindView(R.id.empty_view)
    View emptyView;


    private Unbinder unbinder;
    private FirebaseFirestore firestore;
    FirebaseAuth mAuth;
    Activity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notifications, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        unbinder = ButterKnife.bind(this, view);
        activity = getActivity();

        mAuth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        FirebaseFirestore.getInstance().collection("UsersList")
                .document(FirebaseAuth.getInstance()
                        .getCurrentUser().getUid()).get().addOnSuccessListener(documentSnapshot ->
                getUsersNotificationsList(documentSnapshot.toObject(User.class))
        ).addOnFailureListener(e -> Log.e("NotificationsFragment", e.getMessage()));

        swipeContainer.setOnRefreshListener(() -> {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            NotificationsFragment newFragment = new NotificationsFragment();

            ft.replace(R.id.container, newFragment, "NotificationFragment");
            ft.commit();

            swipeContainer.setRefreshing(false);
        });
        swipeContainer.setColorSchemeResources(R.color.colorPrimary);
    }

    private void getUsersNotificationsList(User user) {
        firestore.collection("Cities").document(Transliterator.transiterate(user.getCity())).collection("Notifications")
                .addSnapshotListener((queryDocumentSnapshots, e) -> {
            if (e != null | queryDocumentSnapshots == null) {
                Log.e("Error:", e.getMessage());
            } else {
                List<Notification> notifications = NotificationFilter.filterNotifications(queryDocumentSnapshots.toObjects(Notification.class), user);
                if (notifications.isEmpty()) {
                    emptyView.setVisibility(View.VISIBLE);
                } else {
                    recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setAdapter(new NotificationsAdapter(activity, notifications, user));
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
