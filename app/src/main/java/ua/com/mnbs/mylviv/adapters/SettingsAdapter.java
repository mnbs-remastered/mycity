package ua.com.mnbs.mylviv.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.com.mnbs.mylviv.R;
import ua.com.mnbs.mylviv.activities.BaseActivity;
import ua.com.mnbs.mylviv.fragments.SettingsFragment;
import ua.com.mnbs.mylviv.managers.FragmentManager;
import ua.com.mnbs.mylviv.models.Settings;

public class SettingsAdapter extends ArrayAdapter<Settings> {

    private Activity mContext;
    private HashMap<String, Boolean> settingsHashMap = new HashMap<>();
    private ArrayList<Settings> settingsList;
    private ArrayList<Boolean> checkedList = new ArrayList<>();

    public SettingsAdapter(Activity context, ArrayList<Settings> settingsList) {
        super(context, 0, settingsList);
        mContext = context;
        this.settingsList = settingsList;
    }

    public void setSettingsHashMap(HashMap<String, Boolean> settingsHashMap) {
        this.settingsHashMap = settingsHashMap;

        for (int i = 0; i < settingsList.size(); i++) {
            Settings settings = settingsList.get(i);
            String title = mContext.getString(settings.getProblemId());
            checkedList.add(settingsHashMap.get(title));
        }
    }

    public HashMap<String, Boolean> getSettingsHashMap() {
        return this.settingsHashMap;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SettingsAdapter.ViewHolder holder;

        if (convertView != null) {
            holder = (SettingsAdapter.ViewHolder) convertView.getTag();
        } else {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.settings_list_item, parent, false);
            holder = new SettingsAdapter.ViewHolder(convertView);
            convertView.setTag(holder);
        }

        Settings currentSetting = getItem(position);
        String currentSettingTitle = mContext.getString(currentSetting.getProblemId());

        holder.problemTextView.setText(currentSettingTitle);

        holder.problemCheckbox.setTag(Integer.valueOf(position));
        try {
            holder.problemCheckbox.setChecked(checkedList.get(position));
        } catch (NullPointerException e) {
            holder.problemCheckbox.setChecked(false);
        }

        holder.imageView.setImageResource(currentSetting.getProblemIco());
        if (mContext instanceof BaseActivity) {
            holder.problemCheckbox.setOnCheckedChangeListener((buttonView, isChecked) -> ((SettingsFragment) FragmentManager.getCurrentFragment((BaseActivity) mContext)).setVisibilityOfButtonChanges());
        }
        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.settings_list_item_problem) TextView problemTextView;
        @BindView(R.id.settings_problem_ico) ImageView imageView;
        @BindView(R.id.settings_checkbox_problem) CheckBox problemCheckbox;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

    }
}
