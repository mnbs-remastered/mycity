package ua.com.mnbs.mylviv.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ua.com.mnbs.mylviv.R;
import ua.com.mnbs.mylviv.activities.BaseActivity;

public class AboutUsFragment extends Fragment {
    @BindView(R.id.about_us_text_view) TextView aboutUs;

    private Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((BaseActivity) getActivity())
                .setActionBarTitle(R.string.about_us_title);
        return inflater.inflate(R.layout.fragment_about_us, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        unbinder = ButterKnife.bind(this, view);
        String sourceString ="Ми команда " + "<b>" + "M&B's" + "</b> " + " - студенти освітнього напряму IoT НУЛП";
        aboutUs.setText(Html.fromHtml(sourceString));
    }
}
