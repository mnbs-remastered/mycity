package ua.com.mnbs.mylviv.models;

import android.util.Log;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class User implements Serializable {

    private String name;
    private String email;
    private int avatarID;
    private String id;
    private Date birthday;
    private String sex;
    private String city;
    private String phoneNumber;
    private String notificationToken;


    private ArrayList<String> addresses = new ArrayList<>();

    private ArrayList<String> complaintsIds = new ArrayList<>();

    private HashMap<String, Boolean> settings = new HashMap<>();

    private String imageUrl;

    private int solvedProblemsNumber;

    private static final int NO_IMAGE_PROVIDED = -1;

    public User(String name, String email, String imageUrl, int solvedProblemsNumber) {
        this.email = email;
        this.name = name;
        this.imageUrl = imageUrl;
        this.solvedProblemsNumber = solvedProblemsNumber;
    }

    public User(String name, String email, int avatarID, int solvedProblemsNumber) {
        this.name = name;
        this.email = email;
        this.avatarID = avatarID;
        this.solvedProblemsNumber = solvedProblemsNumber;
    }

    public User(String name, int avatarID, int solvedProblemsNumber) {
        this(name, null, avatarID, solvedProblemsNumber);
    }

    public User() {
    }

    public HashMap<String,Object> toHashMap() {
        HashMap<String, Object> objectHashMap = new HashMap<>();
        objectHashMap.put("name", this.name);
        objectHashMap.put("email", this.email);
        objectHashMap.put("sex", this.sex);
        objectHashMap.put("birthday", this.birthday);
        objectHashMap.put("imageUrl", this.imageUrl);
        objectHashMap.put("city", this.city);
        objectHashMap.put("phoneNumber", this.phoneNumber);
        return objectHashMap;
    }

    public void setLoggedIn(boolean loggedIn) {
        loggedIn = loggedIn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAvatarID() {
        return avatarID;
    }

    public void setAvatarID(int avatarID) {
        this.avatarID = avatarID;
    }

    public boolean hasImage() {
        return this.avatarID != NO_IMAGE_PROVIDED;
    }

    public int getSolvedProblemsNumber() {
        return solvedProblemsNumber;
    }

    public void setSolvedProblemsNumber(int solvedProblemsNumber) {
        this.solvedProblemsNumber = solvedProblemsNumber;
    }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }


    public ArrayList<String> getComplaintsIds() {
        return complaintsIds;
    }

    public void setComplaintsIds(ArrayList<String> complaintsIds) {
        this.complaintsIds = complaintsIds;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public void setBirthdayFromStr(String birthdayStr) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            this.birthday = sdf.parse(birthdayStr);
        } catch (ParseException | NullPointerException e) {
            Log.e("ERROR", "Failed to read birthday");
        }
    }
    public String extractBirthdayStr(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(this.birthday);
    }

    public ArrayList<String> getAddresses() {
        return addresses;
    }

    public void setAddresses(ArrayList<String> addresses) {
        this.addresses = addresses;
    }

    public HashMap<String, Boolean> getSettings() {
        return settings;
    }

    public void setSettings(HashMap<String, Boolean> settings) {
        this.settings = settings;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getNotificationToken() {
        return notificationToken;
    }

    public void setNotificationToken(String notificationToken) {
        this.notificationToken = notificationToken;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
