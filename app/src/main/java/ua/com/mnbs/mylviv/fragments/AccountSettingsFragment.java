package ua.com.mnbs.mylviv.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ua.com.mnbs.mylviv.R;
import ua.com.mnbs.mylviv.activities.AdditionalRegistrationActivity;
import ua.com.mnbs.mylviv.activities.BaseActivity;
import ua.com.mnbs.mylviv.managers.DateManager;
import ua.com.mnbs.mylviv.managers.FragmentManager;
import ua.com.mnbs.mylviv.managers.ImageSetter;
import ua.com.mnbs.mylviv.managers.InternetChecker;
import ua.com.mnbs.mylviv.managers.KeyboardHider;
import ua.com.mnbs.mylviv.managers.PathGetter;
import ua.com.mnbs.mylviv.models.City;
import ua.com.mnbs.mylviv.models.User;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getApplicationContext;

public class AccountSettingsFragment extends Fragment {

    public static final int GALLERY_REQUEST = 1;
    public static final int STORAGE_PERMISSION_GIVEN = 15;
    private static final int RC_SIGN_IN = 91;
    @BindView(R.id.account_picture)
    ImageView accountPicture;
    @BindView(R.id.linkAccountWithGoogleButton)
    SignInButton googleMergeButton;
    @BindView(R.id.account_name)
    EditText accountName;
    @BindView(R.id.account_email)
    EditText accountEmail;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.sex)
    Spinner sexSpinner;
    @BindView(R.id.btn_merge_facebook)
    LoginButton facebookMergeButton;
    @BindView(R.id.account_city)
    Spinner accountCity;
    @BindView(R.id.account_settings_layout)
    LinearLayout accountSettingsLayout;
    @BindView(R.id.save_changes)
    FloatingActionButton saveChanges;
    @BindView(R.id.account_phone_number)
    EditText accountPhone;

    @BindView(R.id.birthday_date)
    EditText birthdayEditText;
    StorageReference myRef;
    String userId;
    String imageUrl;
    private boolean imageChanged = false;
    private GoogleApiClient mGoogleApiClient;
    private DocumentReference firestore;
    private User currentUser;
    private Unbinder unbinder;
    private Uri imageUri;
    private FirebaseAuth mAuth;
    private Bitmap selectedImage;
    CallbackManager mCallbackManager;
    private boolean linked;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity())
                    .setActionBarTitle(R.string.account_settings_title);
        }
        return inflater.inflate(R.layout.fragment_account_settings, null);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        unbinder = ButterKnife.bind(this, view);

        mAuth = FirebaseAuth.getInstance();
        setCachedData();
        final ArrayAdapter<City> citiesSpinnerArrayAdapter = new ArrayAdapter<City>(
                getApplicationContext(), R.layout.spinner_item, City.values()) {
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                tv.setBackgroundColor(Color.parseColor("#FFFFFF"));
                if (position == 0) {
                    tv.setBackgroundColor(Color.parseColor("#E1E1E1"));
                }
                return view;
            }
        };

        accountCity.setPrompt("Оберіть місто");

        citiesSpinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        accountCity.setAdapter(citiesSpinnerArrayAdapter);

        accountCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });

        linked = false;
        if (mAuth.getCurrentUser().getProviders().contains("google.com")) {
            facebookMergeButton.setVisibility(View.GONE);
            if (mAuth.getCurrentUser().getProviders().size() == 1) {
                googleMergeButton.setVisibility(View.GONE);
            }
            linked = true;
        } else {
            if (mAuth.getCurrentUser().getProviders().size() > 1) {
                mCallbackManager = CallbackManager.Factory.create();
                facebookMergeButton.setReadPermissions();

                facebookMergeButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.e("TAG", "facebook:onSuccess:" + loginResult);
                        handleFacebookAccessToken(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        Log.e("TAG", "facebook:onCancel");
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.e("TAG", "facebook:onError", error);
                        Toast.makeText(getActivity(), R.string.login_failed + error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        }

        if (mAuth.getCurrentUser().getProviders().contains("facebook.com")) {
            googleMergeButton.setVisibility(View.GONE);
            if (mAuth.getCurrentUser().getProviders().size() == 1) {
                facebookMergeButton.setVisibility(View.GONE);
            }
            linked = true;
        }

        firestore = FirebaseFirestore.getInstance().collection("UsersList")
                .document(mAuth.getCurrentUser().getUid());

        String[] sex = new String[]{
                "Обрати стать",
                "Чоловіча",
                "Жіноча"
        };

        final ArrayList<String> sexList = new ArrayList<>(Arrays.asList(sex));

        final ArrayAdapter<String> sexSpinnerArrayAdapter = new ArrayAdapter<String>(
                getApplicationContext(), R.layout.spinner_item, sexList) {
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position % 2 == 1) {
                    tv.setBackgroundColor(Color.parseColor("#FFFFFF"));
                } else {
                    tv.setBackgroundColor(Color.parseColor("#FFFFFF"));
                }
                if (position == 0) {
                    tv.setBackgroundColor(Color.parseColor("#E1E1E1"));
                }
                return view;
            }
        };

        sexSpinner.setPrompt("Оберіть стать");

        sexSpinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        sexSpinner.setAdapter(sexSpinnerArrayAdapter);

        sexSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        firestore.addSnapshotListener((documentSnapshot, e) -> {
            if (InternetChecker.isInternetAvailable()) {
                accountName.setText(mAuth.getCurrentUser().getDisplayName());
                accountEmail.setText(mAuth.getCurrentUser().getEmail());
                currentUser = documentSnapshot.toObject(User.class);
                String currentSex = "";
                try {
                    birthdayEditText.setText(Objects.requireNonNull(currentUser.extractBirthdayStr()));
                } catch (NullPointerException ex) {
                }
                try {
                    currentSex = Objects.requireNonNull(currentUser.getSex());
                } catch (NullPointerException exc) {
                }
                try {
                    accountPhone.setText(currentUser.getPhoneNumber());
                } catch (NullPointerException ex) {

                }

                String currentCity = currentUser.getCity();
                setSpinnerCity(currentCity);
                setSpinnerSex(currentSex);

                if (currentUser.getImageUrl() != null) {
                    imageUri = Uri.parse(currentUser.getImageUrl());
                    ImageSetter.forUsers().setImage(imageUri, accountPicture);

                    if (getActivity() instanceof  BaseActivity) {
                        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
                        ImageView toolbarAvatar = toolbar.findViewById(R.id.avatar);
                        ImageSetter.forUsers().setImage(imageUri, toolbarAvatar);
                    }
                } else {
                    accountPicture.setImageResource(R.drawable.user);
                }
            }
        });
        userId = mAuth.getCurrentUser().getUid();

        myRef = FirebaseStorage.getInstance().getReference().child("UserPhotos/" + userId);
    }

    @OnClick(R.id.linkAccountWithGoogleButton)
    public void linkAccountWithGoogle() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder((GoogleSignInOptions.DEFAULT_SIGN_IN))
                .requestIdToken(getString(R.string.default_web_client_id))
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity()).
                enableAutoManage(getActivity(), connectionResult ->
                        Toast.makeText(getActivity(), "Немає з'єднання з інтернетом", Toast.LENGTH_LONG)
                                .show()).
                addApi(Auth.GOOGLE_SIGN_IN_API, gso).
                build();

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);

        progressBar.setVisibility(ProgressBar.VISIBLE);
        accountSettingsLayout.setVisibility(LinearLayout.INVISIBLE);
    }

    private void setSpinnerCity(String currentCity) {
        if (currentCity != null) {
            if (currentCity.equals(City.NOCITY.toString())) {
                accountCity.setSelection(0);
            } else if (currentCity.equals(City.LVIV.toString())) {
                accountCity.setSelection(1);
            } else if (currentCity.equals(City.KYIV.toString())) {
                accountCity.setSelection(2);
            } else if (currentCity.equals(City.DNIPRO.toString())) {
                accountCity.setSelection(3);
            }
        }
    }

    private void setSpinnerSex(String currentSex) {
        if (currentSex.equals("Чоловіча")) {
            sexSpinner.setSelection(1);
        } else if (currentSex.equals("Жіноча")) {
            sexSpinner.setSelection(2);
        }
    }

    private void setCachedData() {
        SharedPreferences prefs = getActivity().getSharedPreferences("UserInfo", Activity.MODE_PRIVATE);

        accountName.setText(prefs.getString("name", ""));
        accountEmail.setText(prefs.getString("email", ""));
        birthdayEditText.setText(prefs.getString("birthday", ""));
        accountPhone.setText(prefs.getString("phoneNumber", ""));
        imageUrl = prefs.getString("uri", "");
        if (!imageUrl.equals("")) {
            ImageSetter.forUsers().setImage(imageUrl, accountPicture);

            Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
            ImageView toolbarAvatar = toolbar.findViewById(R.id.avatar);
            ImageSetter.forUsers().setImage(imageUrl, toolbarAvatar);
        }

        String sex = prefs.getString("sex", "");
        setSpinnerSex(sex);

        String city = prefs.getString("city", "");
        setSpinnerCity(city);

        //TODO: add image caching
    }

    @OnClick(R.id.account_picture)
    public void changePicture() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
            requestPermissions(permissions, STORAGE_PERMISSION_GIVEN);
        } else {
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
        }
    }

    @OnClick(R.id.save_changes)
    public void saveChanges() {

        if (!InternetChecker.isInternetAvailable()) {
            Toast.makeText(getActivity(), "Підключіться до Інтернету", Toast.LENGTH_LONG).show();
        } else {
            accountSettingsLayout.setVisibility(View.INVISIBLE);
            String newName = accountName.getText().toString();
            String newEmail = accountEmail.getText().toString();
            String newBirthday = birthdayEditText.getText().toString();
            String sex = sexSpinner.getSelectedItem().toString().trim();
            String newCity = accountCity.getSelectedItem().toString();
            String newPhone = accountPhone.getText().toString();

            progressBar.setVisibility(View.VISIBLE);

            if (newName.equals("") || newEmail.equals("")) {
                Toast.makeText(getActivity(), "Введіть дані", Toast.LENGTH_LONG).show();
            } else {
                UserProfileChangeRequest.Builder profileChangeRequest = new UserProfileChangeRequest.Builder();
                progressBar.setVisibility(View.VISIBLE);
                accountSettingsLayout.setVisibility(View.INVISIBLE);
                if (imageChanged) {
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    if (selectedImage != null) {
                        selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                    }
                    byte[] bitmapdata = bos.toByteArray();
                    ByteArrayInputStream bs = new ByteArrayInputStream(bitmapdata);
                    myRef.putStream(bs).addOnSuccessListener(taskSnapshot ->
                            myRef.getDownloadUrl().addOnSuccessListener(uri ->
                                    firestore.update("imageUrl", uri.toString()).addOnSuccessListener(taskSnapshot1 ->
                                            profileChangeRequest.setPhotoUri(uri)
                                    )
                            )).addOnFailureListener(e ->
                            Toast.makeText(getActivity(), "Виникла помилка:" + e.getMessage(), Toast.LENGTH_LONG).show()
                    );
                }
                if (!currentUser.getName().equals(newName)) {
                    profileChangeRequest.setDisplayName(newName);
                }

                if (!currentUser.getEmail().equals(newEmail)) {
                    mAuth.getCurrentUser()
                            .updateEmail(newEmail)
                            .addOnSuccessListener(task ->
                                    Log.i("CHANGES", "Email successfully changed"));
                }
                HashMap<String, Object> current = currentUser.toHashMap();
                HashMap<String, Object> update = new HashMap<>();
                current.remove("imageUrl");
                update.put("name", newName);
                update.put("email", newEmail);
                update.put("sex", sex);
                try {
                    update.put("birthday", DateManager.extractDateFromString(newBirthday));
                } catch (ParseException e) {
                    Log.i("Account Settings", e.getMessage());
                }
                update.put("city", newCity);
                update.put("phoneNumber", newPhone);

                if (current.equals(update) && !imageChanged) {
                    progressBar.setVisibility(ProgressBar.GONE);
                    accountSettingsLayout.setVisibility(View.VISIBLE);
                    return;
                }
                mAuth.getCurrentUser()
                        .updateProfile(profileChangeRequest.build())
                        .addOnSuccessListener(task -> {
                            currentUser.setBirthdayFromStr(newBirthday);
                            Log.i("CHANGES", "Changes applied in Firebase");
                            firestore.update(update).addOnSuccessListener(task1 -> {
                                Toast.makeText(getActivity(), "Дані змінено", Toast.LENGTH_LONG).show();

                                SharedPreferences preferences = getActivity()
                                        .getSharedPreferences("UserInfo", getActivity().MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("name", newName);
                                editor.putString("email", newEmail);
                                editor.putString("sex", sex);
                                editor.putString("city", newCity);
                                editor.putString("birthday", newBirthday);
                                editor.putString("imageUrl", imageUri.toString());
                                editor.putString("phoneNumber", newPhone);
                                editor.apply();

                                myRef.getDownloadUrl().addOnSuccessListener(uri ->
                                        firestore.update("imageUrl", uri.toString()).addOnSuccessListener(aVoid -> {
                                            progressBar.setVisibility(View.GONE);
                                            accountSettingsLayout.setVisibility(View.VISIBLE);

                                            Fragment fragment = new NotificationsFragment();
                                            FragmentManager.open(fragment, (BaseActivity) getActivity());
                                            ((BaseActivity) getActivity()).navigation
                                                    .getMenu().findItem(R.id.navigation_notifications).setChecked(true);
                                        })
                                );
                            });
                        });
                KeyboardHider.hideKeyboard(getActivity());
            }
            accountSettingsLayout.setVisibility(LinearLayout.INVISIBLE);
            if (getActivity() instanceof AdditionalRegistrationActivity) {
                Intent toBaseActivity = new Intent(getActivity(), BaseActivity.class);
                startActivity(toBaseActivity);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_GIVEN) {
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
        }
    }

    @SuppressLint("SetTextI18n")
    @OnClick({R.id.birthday_date, R.id.calendar_icon_account})
    public void getBirthday() {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View calendarDialogView = inflater.inflate(R.layout.dialog_calendar, null);
        alertDialog.setView(calendarDialogView);
        Calendar calendarMin, calendarMax;
        calendarMin = Calendar.getInstance();
        calendarMax = Calendar.getInstance();
        calendarMin.add(Calendar.YEAR, -80);
        calendarMax.add(Calendar.YEAR, -18);
        DatePicker datePicker = calendarDialogView.findViewById(R.id.datePicker);
        datePicker.setMinDate(calendarMin.getTimeInMillis());
        datePicker.setMaxDate(calendarMax.getTimeInMillis());
        calendarDialogView.findViewById(R.id.dialog_save).setOnClickListener(v -> {
            int year = datePicker.getYear();
            int monthOfYear = datePicker.getMonth();
            int dayOfMonth = datePicker.getDayOfMonth();

            birthdayEditText.setText(((dayOfMonth >= 10) ? "" : "0") + dayOfMonth + "/"
                    + ((monthOfYear >= 9) ? "" : "0") + (monthOfYear + 1) + "/" + year);
            alertDialog.dismiss();
        });
        calendarDialogView.findViewById(R.id.dialog_dismiss).setOnClickListener(v -> {
            alertDialog.dismiss();
        });
        alertDialog.show();
    }

    private void authWithGoogle(GoogleSignInAccount account) {
        if (linked) {
            unlinkProvider("google.com");
        } else {
            AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
            mAuth.getCurrentUser().linkWithCredential(credential).addOnSuccessListener(task -> {
                progressBar.setVisibility(ProgressBar.INVISIBLE);
                accountSettingsLayout.setVisibility(View.VISIBLE);
                Toast.makeText(getActivity(), "Ваш акаунт приєднано до Google", Toast.LENGTH_LONG).show();
            }).addOnFailureListener(e -> {
                progressBar.setVisibility(ProgressBar.INVISIBLE);
                accountSettingsLayout.setVisibility(View.VISIBLE);
                Toast.makeText(getActivity(), "Сталася помилка:" + e.getMessage(), Toast.LENGTH_LONG).show();
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST) {
            if (resultCode == getActivity().RESULT_OK) {
                try {
                    imageUri = data.getData();
                    final InputStream imageStream = getActivity().getContentResolver().openInputStream(imageUri);
                    Bitmap image = BitmapFactory.decodeStream(imageStream);
                    float height = image.getHeight();
                    float width = image.getWidth();

                    ExifInterface exif = new ExifInterface(PathGetter.getPath(getActivity(), imageUri));
                    int rotation = exif.getAttributeInt(
                            ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                    int rotationInDegrees = convertToDegrees(rotation);
                    Matrix matrix = new Matrix();
                    if (rotation != 0) {
                        matrix.preRotate(rotationInDegrees);
                    }

                    Bitmap selectedImageBeforeScaling = Bitmap.createBitmap(image, 0, 0, (int) width,
                            (int) height, matrix, true);

                    float proportion = width / height;

                    try {
                        if (proportion > 1) {
                            selectedImage = Bitmap.createScaledBitmap(selectedImageBeforeScaling, Math.round(400 * proportion), 400, false);
                        } else {
                            selectedImage = Bitmap.createScaledBitmap(selectedImageBeforeScaling, 400, Math.round(400 / proportion), false);
                        }
                    } catch (NullPointerException e) {
                        selectedImage = selectedImageBeforeScaling;
                    }

                    accountPicture.setImageBitmap(selectedImage);
                    imageChanged = true;
                } catch (IOException e) {
                    Toast.makeText(getActivity(), "No image provided", Toast.LENGTH_LONG).show();
                }
            }
        } else if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                GoogleSignInAccount account = result.getSignInAccount();
                authWithGoogle(account);
                progressBar.setVisibility(ProgressBar.VISIBLE);
                accountSettingsLayout.setVisibility(View.INVISIBLE);
            } else {
                Toast.makeText(getActivity(), "Помилка входу:" + Auth.GoogleSignInApi.getSignInResultFromIntent(data)
                        .getStatus().toString(), Toast.LENGTH_LONG).show();
                progressBar.setVisibility(ProgressBar.INVISIBLE);
                accountSettingsLayout.setVisibility(View.VISIBLE);
            }
        } else mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void handleFacebookAccessToken(AccessToken token) {
        if (linked) {
            unlinkProvider("facebook.com");
        } else {
            Log.d("TAG", "handleFacebookAccessToken:" + token);
            AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
            mAuth.getCurrentUser().linkWithCredential(credential).addOnSuccessListener(authResult ->
                    Toast.makeText(getActivity(), "Ваш акаунт тепер приєднано до Facebook", Toast.LENGTH_LONG).show()
            ).addOnFailureListener(e -> Toast.makeText(getActivity(), "Помилка приєднання:"
                    + e.getMessage(), Toast.LENGTH_LONG).show());
        }
    }

    private void unlinkProvider(String provider) {
        mAuth.getCurrentUser().unlink(provider).addOnSuccessListener(task -> {
            progressBar.setVisibility(ProgressBar.INVISIBLE);
            accountSettingsLayout.setVisibility(View.VISIBLE);
            Toast.makeText(getActivity(), "Ваш акаунт від'єднано від Google", Toast.LENGTH_LONG).show();
        }).addOnFailureListener(e -> {
            progressBar.setVisibility(ProgressBar.INVISIBLE);
            accountSettingsLayout.setVisibility(View.VISIBLE);
            Toast.makeText(getActivity(), "Сталася помилка:" + e.getMessage(), Toast.LENGTH_LONG).show();
        });
    }

    private int convertToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

}
