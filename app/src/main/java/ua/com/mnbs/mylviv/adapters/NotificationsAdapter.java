package ua.com.mnbs.mylviv.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.com.mnbs.mylviv.R;
import ua.com.mnbs.mylviv.models.Notification;
import ua.com.mnbs.mylviv.models.User;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {

    private List<Notification> mNotifications;
    private static Activity mContext;
    private User user;

    public NotificationsAdapter(Activity context, List<Notification> notificationsLists, User currentUser) {
        Collections.reverse(notificationsLists);
        mContext = context;
        user = currentUser;
        mNotifications = notificationsLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.notifications_list_item, viewGroup, false);
        return new NotificationsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {

        Notification currentNotification = mNotifications.get(i);

        try {
            holder.regionTextView.setText(currentNotification.getRegion());
            holder.localityTextView.setText(currentNotification.getTown());
            holder.streetTextView.setText(currentNotification.getStreet());
            holder.houseTextView.setText(currentNotification.getHouses());
            holder.periodTextView.setText("з " + getFancyDate(currentNotification.getBeginDate()) + " до " + getFancyDate(currentNotification.getEndDate()));
            holder.infoTextView.setText(currentNotification.getType());
            holder.notificationsPeriodBrief.setText("з " + getFancyDate(currentNotification.getBeginDate()) + " до " + getFancyDate(currentNotification.getEndDate()));
            holder.itemView.setOnClickListener(v -> {
                if (holder.notificationHouseLayout.getVisibility() == View.GONE) {
                    holder.notificationHouseLayout.setVisibility(View.VISIBLE);
                    holder.notificationPeriodLayout.setVisibility(View.VISIBLE);
                    holder.notificationLocationLayout.setVisibility(View.VISIBLE);
                    holder.notificationsPeriodBrief.setVisibility(View.GONE);
                    holder.infoTextView.setVisibility(View.VISIBLE);
                }
                else {
                    holder.notificationHouseLayout.setVisibility(View.GONE);
                    holder.notificationPeriodLayout.setVisibility(View.GONE);
                    holder.notificationLocationLayout.setVisibility(View.GONE);
                    holder.notificationsPeriodBrief.setVisibility(View.VISIBLE);
                    holder.infoTextView.setVisibility(View.GONE);
                }
            });

            String typeTextView = holder.infoTextView.getText().toString();
            holder.notificationTypeImage.setVisibility(View.VISIBLE);
            if (typeTextView.equals(mContext.getString(R.string.water_cut_off))) {
                holder.notificationTypeImage.setImageResource(R.drawable.water);
            } else if (typeTextView.equals(mContext.getString(R.string.electricity_cut_off))) {
                holder.notificationTypeImage.setImageResource(R.drawable.electricity);
            } else if (typeTextView.equals(mContext.getString(R.string.gas_cut_off))) {
                holder.notificationTypeImage.setImageResource(R.drawable.gas);
            } else if (typeTextView.equals(mContext.getString(R.string.road_problems))) {
                holder.notificationTypeImage.setImageResource(R.drawable.road);
            } else if (typeTextView.equals(mContext.getString(R.string.transport_problems))) {
                holder.notificationTypeImage.setImageResource(R.drawable.transport);
            } else if (typeTextView.equals(mContext.getString(R.string.important_events))) {
                holder.notificationTypeImage.setImageResource(R.drawable.importantevents);
            }
        } catch (NullPointerException e) {
            Log.e("Notification", "hasn't been set");
        }
        setFadeAnimation(holder.itemView);
    }


    private void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }

    @Override
    public int getItemCount() {
        return mNotifications.size();
    }

    private String pad(int n) {
        return (n < 10) ? ("0" + n) : "" + n;
    }

    private String getFancyDate(Date date) {
        return pad(date.getHours()) + ":" + pad(date.getMinutes())
              + " " + pad(date.getDay()) + "." + pad(date.getMonth() + 1) + ".20" + (date.getYear()-100);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.notifications_region)
        TextView regionTextView;
        @BindView(R.id.notifications_locality)
        TextView localityTextView;
        @BindView(R.id.notifications_street)
        TextView streetTextView;
        @BindView(R.id.notifications_house)
        TextView houseTextView;
        @BindView(R.id.notifications_period)
        TextView periodTextView;
        @BindView(R.id.notifications_info)
        TextView infoTextView;
        @BindView(R.id.delete)
        View deleteView;
        @BindView(R.id.notifications_list_item_current_notification)
        ConstraintLayout currentNotificationView;
        @BindView(R.id.notification_image)
        ImageView notificationTypeImage;
        @BindView(R.id.notification_period_layout)
        LinearLayout notificationPeriodLayout;
        @BindView(R.id.notifications_location_layout)
        LinearLayout notificationLocationLayout;
        @BindView(R.id.notifications_house_layout)
        LinearLayout notificationHouseLayout;
        @BindView(R.id.notifications_period_brief)
        TextView notificationsPeriodBrief;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(v -> {
                Log.d("RecyclerView", "onClick：" + getAdapterPosition());
                currentNotificationView.removeAllViews();
            });
            ButterKnife.bind(this, view);
        }

        @OnClick(R.id.delete)
        public void deleteNotification() {
            AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
            alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            LayoutInflater inflater = LayoutInflater.from(mContext);
            View deleteDialogView = inflater.inflate(R.layout.dialog, null);
            alertDialog.setView(deleteDialogView);

            TextView dialogQuestion = deleteDialogView.findViewById(R.id.dialog_question);
            dialogQuestion.setText(R.string.delete_notification);

            deleteDialogView.findViewById(R.id.dialog_yes).setOnClickListener(v -> {
                currentNotificationView.removeAllViews();
                alertDialog.dismiss();
            });
            deleteDialogView.findViewById(R.id.dialog_no).setOnClickListener(v -> alertDialog.dismiss());
            alertDialog.show();
        }
    }
}