package ua.com.mnbs.mylviv.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ua.com.mnbs.mylviv.R;
import ua.com.mnbs.mylviv.activities.BaseActivity;
import ua.com.mnbs.mylviv.adapters.TopUsersAdapter;
import ua.com.mnbs.mylviv.managers.Transliterator;
import ua.com.mnbs.mylviv.models.User;

public class TopUsersFragment extends Fragment {

    public static final int TOP_USERS_NUMBER = 30;
    private Unbinder unbinder;
    CollectionReference firestore;

    @BindView(R.id.top_user_list)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((BaseActivity) getActivity())
                .setActionBarTitle(R.string.top_users_title);
        return inflater.inflate(R.layout.fragment_top_users, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        unbinder = ButterKnife.bind(this, view);

        firestore = FirebaseFirestore.getInstance().collection("UsersList");

        firestore.document(FirebaseAuth.getInstance().getCurrentUser().getUid()).get().addOnSuccessListener( documentSnapshot ->
                getListFromSource((String) documentSnapshot.get("city"))
                );
        progressBar.setVisibility(ProgressBar.INVISIBLE);

        swipeContainer.setOnRefreshListener(() -> {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            FragmentManager fm = getActivity().getSupportFragmentManager();
            TopUsersFragment newFragment = new TopUsersFragment();

            ft.replace(R.id.container, newFragment, "MyComplaintsFragment");
            ft.addToBackStack(null);
            ft.commit();
            fm.popBackStack();
            swipeContainer.setRefreshing(false);
        });

        swipeContainer.setColorSchemeResources(R.color.colorPrimary);
    }

    private void getListFromSource(String city) {
        firestore.orderBy("solvedProblemsNumber", Query.Direction.DESCENDING)
                .limit(TOP_USERS_NUMBER).get().addOnSuccessListener(queryDocumentSnapshots -> {
            ArrayList<User> topUsers = (ArrayList<User>) queryDocumentSnapshots.toObjects(User.class);
            if (getContext() != null) {
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                recyclerView.setHasFixedSize(true);
                recyclerView.setAdapter(new TopUsersAdapter(getActivity(), topUsers));
            }
        }).addOnFailureListener(e ->
                Log.e("ERROR", "shit happens:" + e.getMessage(), e)
        );
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

}
