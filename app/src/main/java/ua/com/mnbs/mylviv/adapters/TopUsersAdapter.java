package ua.com.mnbs.mylviv.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.com.mnbs.mylviv.R;
import ua.com.mnbs.mylviv.activities.BaseActivity;
import ua.com.mnbs.mylviv.managers.ImageSetter;
import ua.com.mnbs.mylviv.models.User;

import static android.widget.Toast.LENGTH_LONG;

public class TopUsersAdapter extends RecyclerView.Adapter<TopUsersAdapter.ViewHolder> {

    private Activity mContext;
    private ArrayList<User> mTopUsers;

    private StorageReference myRef;

    private String mUid;

    public TopUsersAdapter(Activity context, ArrayList<User> usersList) {
        mContext = context;
        mTopUsers = usersList;
        mUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.top_users_list_item, viewGroup, false);
        return new TopUsersAdapter.ViewHolder(v);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        User currentUser = mTopUsers.get(position);

        String posInString = (position + 1) + ".";

        if (position<=2) {
            holder.cupImageView.setVisibility(View.VISIBLE);
            switch (position) {
                case 0:
                    holder.cupImageView.setImageResource(R.drawable.top1);
                    break;
                case 1:
                    holder.cupImageView.setImageResource(R.drawable.top2);
                    break;
                case 2:
                    holder.cupImageView.setImageResource(R.drawable.top3);
                    break;
            }
        } else {
            holder.numberTextView.setVisibility(View.VISIBLE);
        }

        holder.numberTextView.setText(posInString);
        holder.userNameTextView.setText(currentUser.getName());
        holder.quantityOfProblemsTextView.setText(String.valueOf(currentUser.getSolvedProblemsNumber()));

        String uid = currentUser.getId();
        myRef = FirebaseStorage.getInstance().getReference().child("UserPhotos/" + uid);

        if (currentUser.getImageUrl() != null) {
            ImageSetter.forUsers().setImage(currentUser.getImageUrl(), holder.imageView);
        }

        if (mUid.equals(currentUser.getId())) {
            int greenColorValue = Color.parseColor("#4000C597");
            holder.userLayout.setBackgroundColor(greenColorValue);
        }
        setFadeAnimation(holder.itemView);
    }

    private void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }

    @Override
    public int getItemCount() {
        return mTopUsers.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.top_users_list_item_number) TextView numberTextView;
        @BindView(R.id.top_users_list_item_user_name) TextView userNameTextView;
        @BindView(R.id.top_users_list_item_quantity_of_problems) TextView quantityOfProblemsTextView;
        @BindView(R.id.top_users_list_item_image) ImageView imageView;
        @BindView(R.id.top_users_list_item_cup) ImageView cupImageView;
        @BindView(R.id.progressBar) ProgressBar progressBar;
        @BindView(R.id.top_users_list_item_one)
        FrameLayout userLayout;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}