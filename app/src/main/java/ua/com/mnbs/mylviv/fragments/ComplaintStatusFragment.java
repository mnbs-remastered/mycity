package ua.com.mnbs.mylviv.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ua.com.mnbs.mylviv.R;
import ua.com.mnbs.mylviv.activities.BaseActivity;
import ua.com.mnbs.mylviv.managers.ImageSetter;
import ua.com.mnbs.mylviv.models.Complaint;
import ua.com.mnbs.mylviv.models.Status;

import static android.view.View.GONE;
import static android.view.View.getDefaultSize;

public class ComplaintStatusFragment extends Fragment {

    @BindView(R.id.complaint_status_image)
    ImageView image;
    @BindView(R.id.complaint_status_address)
    TextView address;
    @BindView(R.id.complaint_status_progress_status)
    TextView status;
    @BindView(R.id.complaint_status_responsible_company)
    TextView company;
    @BindView(R.id.complaint_status_description)
    TextView description;
    @BindView(R.id.complaint_status_deadline)
    TextView deadline;
    @BindView(R.id.complaint_status_feedback)
    TextView feedback;

    @BindView(R.id.complaint_status_responsible_company_group)
    LinearLayout companyLayout;
    @BindView(R.id.complaint_status_deadline_group)
    LinearLayout deadlineLayout;
    @BindView(R.id.complaint_status_feedback_group)
    LinearLayout feedbackLayout;

    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().findViewById(R.id.viewpager).setVisibility(GONE);
        String solved = getArguments().getString("solved");
        String my = getArguments().getString("My");
        if (solved == "From SolvedComplaints") {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("solved", "SolvedComplaints");
            editor.remove("My");
            editor.apply();
        } else {

        }
        if (my == "From MyComplaints") {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("My", "MyComplaints");
            editor.remove("solved");
            editor.apply();
        } else {

        }
        Toolbar toolbar = ((BaseActivity) getActivity()).toolbar;
        toolbar.setNavigationIcon(R.drawable.ic_action_name);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
                toolbar.setNavigationIcon(null);
            }
        });
        return inflater.inflate(R.layout.fragment_complaint_status, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        unbinder = ButterKnife.bind(this, view);

        Complaint currentComplaint = (Complaint) getArguments().getSerializable("complaint");

        displayInfo(currentComplaint);
    }

    private void displayInfo(Complaint complaint) {
        ImageSetter.forComplaints().setImage(complaint.getImageUrl(), image);
        address.setText(complaint.getAddress());
        String complaintStatus = complaint.getStatus();
        status.setText(complaintStatus);


        if (complaintStatus.equals(Status.IN_PROGRESS.toString())) {
            company.setText(complaint.getCompany());
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            deadline.setText((sdf.format(complaint.getDeadline())));
            feedbackLayout.setVisibility(GONE);
        } else if (complaintStatus.equals(Status.SOLVED.toString())) {
            company.setText(complaint.getCompany());
            feedback.setText(complaint.getFeedback());
            deadlineLayout.setVisibility(GONE);
        } else {
            companyLayout.setVisibility(GONE);
            deadlineLayout.setVisibility(GONE);
            feedbackLayout.setVisibility(GONE);
        }

        description.setText(complaint.getDescription());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
